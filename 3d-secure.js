module.exports = {
  qrPurchase: require('./create-purchase/3d-secure/qr-purchase'),
  aioPurchase: require('./create-purchase/3d-secure/aio-purchase'),
  wkNewPurchase: require('./payment/3d-sercure/wk-new-purchase'),
  wkDynamicQr: require('./payment/3d-sercure/wk-dynamic-qr'),
  //TODO add other functions related to 3d secure here
}