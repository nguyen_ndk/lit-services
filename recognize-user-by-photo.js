const httpError = require('http-errors')
const {facade: {KycFacade, Log}} = require('lit-utils')
const {errorTags, KYC} = require('lit-constants')
const {KycReqCountingRepo} = require('lit-repositories')
// getUserInfoById
module.exports = async (idPhoto, selfie, userId) => {
  const {data, api} = await KycFacade.getUserInfoByIDImage(idPhoto, selfie)
  const mapped = KycFacade.mapping(data)
  let status, error
  if (!mapped.verified) {
    status = KYC.STATUS.INVALID_ID
    error = httpError(400, errorTags.FAIL_TO_RECOGNIZE_ID)
  } else if (!mapped.selfieCheckMatched) {
    status = KYC.STATUS.ID_AND_SELFIE_MISMATCH
    error = httpError(400, errorTags.ID_AND_SELFIE_MISMATCH)
  } else {
    status = KYC.STATUS.PASSED_SELFIE
  }
  const saveData = {
    userId,
    api,
    provider: mapped.provider,
    status
  }
  KycReqCountingRepo.create(saveData).catch(e => {
    Log.error('SAVE_KYC_RESPONSE', saveData, e)
  })
  if (error) throw error
}