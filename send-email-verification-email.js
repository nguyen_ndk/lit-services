const {Email} = require('lit-notifications')

const {facade: {TokenFacade, Log}} = require('lit-utils')
const {EMAIL_TOKEN_SECRET, EMAIL_TOKEN_EXPIRATION} = require('lit-constants')

module.exports = (email, userId) => {
  const body = { _id: userId }
  const token = TokenFacade.sign(body, EMAIL_TOKEN_SECRET, EMAIL_TOKEN_EXPIRATION)
  Email.sendVerificationEmail(token, email).catch(e => {
    Log.error(`SEND_VERIFICATION_EMAIL user ${userId}`, {error: e})
  })
}