const {facade: {Log}} = require('lit-utils')
const {UserRepo} = require('lit-repositories')

module.exports = async (userId, status) => {
  Log.info(`UPDATE_USER_STATUS user ${userId}`, status)
  try {
    return await UserRepo.update(userId, {status}, {getModel: true})
  } catch (e) {
    Log.error(`UPDATE_USER_STATUS user ${userId}`, e)
  }
}