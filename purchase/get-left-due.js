const {PurchaseLineRepo} = require('lit-repositories')

module.exports = async (purchase) => {

  let amountPaid = await PurchaseLineRepo.getPaidByUserId(purchase.userId, [purchase.id])
  let totalAmountDue = purchase.amount + purchase.adminFeeAmount

  let amountLeftDue = totalAmountDue - amountPaid

  return amountLeftDue
}