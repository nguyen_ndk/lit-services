const httpError = require('http-errors')
const {errorTags, PAYMENT_METHOD, BLOCK_PAYMENT_FEATURE} = require('lit-constants')
const {PaymentMethodRepo,  PurchaseRepo} = require('lit-repositories')

const processors = require('../payment-processor')

const _getProcessor = (paymentMethodType) => {
  switch (paymentMethodType) {
    case PAYMENT_METHOD.TYPE.ATM:
      if (BLOCK_PAYMENT_FEATURE.BLOCK_NAPAS_PAYMENT) {
        throw httpError(403, errorTags.BLOCK_NAPAS_PAYMENT)
      }
      return processors.PayDynamicQrOtp
    case PAYMENT_METHOD.TYPE.CREDIT_CARD:
    case PAYMENT_METHOD.TYPE.DEBIT_CARD:
      if (BLOCK_PAYMENT_FEATURE.BLOCK_CREDIT_PAYMENT) {
        throw httpError(403, errorTags.BLOCK_CREDIT_PAYMENT)
      }
      return processors.PayDynamicQrDirect
    default:
      throw httpError(400, errorTags.INVALID_PAYMENT_TYPE)
  }
}

module.exports = async (req) => {
  const [purchase, pm] = await Promise.all([
    PurchaseRepo.findByIdWithMerchant(req.purchaseId),
    PaymentMethodRepo.findById(req.paymentMethodId)
  ])
  const processor = _getProcessor(pm.type)
  processor.validate(pm, purchase, req.user)
  const calcData = await processor.getAmount(req, purchase)
  const desc = await processor.getDesc(purchase.id, calcData.chargeAmount)
  const result = await processor.processPayment(req, calcData.chargeAmount, pm, desc)
  if (result.isSuccess) {
    // updatePromotion(calcData.promoData, req.user)
    return processor.update(result.data, purchase, pm.id, calcData, req)
  } else {
    processor.failedUpdate(purchase.id, req.user.id)
    throw result.error
  }
}