const httpError = require('http-errors')
const axios = require('axios')
const _ = require('lodash')
const axiosRetry = require('axios-retry')

axiosRetry(axios, { retryDelay: axiosRetry.exponentialDelay})

const {
  PurchaseRepo,
  UserRepo,
  PaymentMethodRepo,
  MerchantRepo,
  PromotionRepo
} = require('lit-repositories')
const {facade: {
  DBFacade,
  QueueFacade,
  Log
}, helpers} = require('lit-utils')
const {
  PURCHASE_PAYMENT_STATUS,
  errorTags,
  QUEUE,
} = require('lit-constants')

const {OnlineStore} = require('lit-models')

const recordUserCreditChange = require('../../record-user-credit-change')
const updateUserCredit = require('../../update-user-credit')
const addSucceededFirstInstallment = require('../../add-succeeded-first-installment')
const charge = require('../../payment/direct/charge')
const purchasePayFirstInstallment = require('../../purchase-pay-first-installment')
const handlePurchaseError = require('../../handle-purchase-error')
const saveInstallmentTranId = require('../../save-installment-tranid')
const {Email} = require('lit-notifications')
const crypto = require('crypto')
var httpBuildQuery = require('http-build-query')

const accountingSyncPurchase = require('../../accounting/sync-purchase')
const updatePromotion  = require('../../promotion/update-after-purchase')
const sendToQueue = (data, userId) => {
  Log.info('USER_CONFIRM_PURCHASE SEND_TO_QUEUE', userId, data)
  const message = JSON.stringify({...data,... {userId}})
  QueueFacade.send(message, {
    queue: QUEUE.CONFIRM_PURCHASE
  }).catch(e => {
    Log.error('USER_CONFIRM_PURCHASE SEND_TO_QUEUE ERROR', e, userId)
  })
}


const verifyPurchaseInValidState = p => {
  if (p.paymentStatus !== PURCHASE_PAYMENT_STATUS.INITIALIZED)
    throw httpError(403, errorTags.PURCHASE_ALREADY_PROCESSED)
}

const notifyMerchant = async (p, body) => {
  try {
    const res = axios.post(p.notifyUrl, body)
    Log.info(`USER_CONFIRM_PURCHASE MERCHANT_NOTIFY purchase ${p.id}`, res, body)
  } catch (e) {
    Log.error(`USER_CONFIRM_PURCHASE MERCHANT_NOTIFY purchase ${p.id}`, e, body)
    // throw httpError(500, errorTags.MERCHANT_SERVER_ERROR)
  }
}

const buildNotifyBody = (reqBody, p, m, error, code) => {


  let notifyData =  {
    clientId: reqBody.clientId,
    requestId: p.merchantRequestId,
    orderId: p.merchantOrderId,
    amount: p.amount,
    transactionId: p.transactionId,
    errorMessage: error,
    errorCode: code,
    payeeAgent: reqBody.payeeAgent,
    responseTime: Date.now() / 1000
  }

  //Digital signature for security Merchant can check if the signature match with their signature
  if (m.OnlineStore){
    const signature = crypto.createHmac('SHA256', m.OnlineStore.accessKey).update(httpBuildQuery(notifyData)).digest('hex')
    notifyData.signature = signature
  }

  return notifyData
}

const checkPurchaseValidity = p => {
  if (!p) throw httpError(404, errorTags.PURCHASE_NOT_FOUND)
  verifyPurchaseInValidState(p)
}

const sendEmail = (email, credit, deducted, pId) => {
  PurchaseRepo.findByIdWithMerchant(pId)
    .then(p => {
      return Email.sendNewPurchase(email, credit, deducted, p, _.get(p, 'Merchant'))
    })
    .catch(e => {
      Log.error(`USER_CONFIRM_PURCHASE NOTIFY_USER user ${email}`, e)
    })
}

module.exports = async (userId, reqBody) => {
  Log.info('USER_CONFIRM_PURCHASE START', userId, reqBody)
  const merReqId = helpers.genMerReqId(reqBody)
  const currentPurchase = await PurchaseRepo.findByMerchantRequestId(merReqId)
  checkPurchaseValidity(currentPurchase)
  const m = await MerchantRepo.findById(currentPurchase.merchantId, { include: [{model: OnlineStore}]})
  const pm = await PaymentMethodRepo.findById(reqBody.paymentMethodId)
  const t = await DBFacade.transaction()
  let returnUrl = _.get(currentPurchase, 'returnUrl', '')
  try {
    const [u, p] = await Promise.all([
      UserRepo.selectForUpdate(userId, t),
      PurchaseRepo.findByMerchantRequestIdForUpdate(
        merReqId,
        {transaction: t}
      )
    ])
    if (!u || !p) {
      await t.rollback()
      sendToQueue(reqBody, userId)
      const body = buildNotifyBody(reqBody, currentPurchase, m, '', 0)
      return {success: false, returnUrl: helpers.addQueriesToUrl(returnUrl, body), code: 202}
    }
    PaymentMethodRepo.verifyPaymentMethod(pm, u)
    verifyPurchaseInValidState(p)
    const purchaseInfo = {
      amount : p.originalAmount,
      userId: userId
    }
    let promoData = await PromotionRepo.verifyPurchase(purchaseInfo, reqBody.voucherCode)
    UserRepo.verifyUserValidity(u)
    UserRepo.checkCreditSufficiency(u, promoData.amountCharge)
    var {amount, chargeAmount, totalInstallments, adminFeeAmount} =
      MerchantRepo.calcInstallment(promoData.amountCharge, m)
    const userCredit = helpers.userCreditAfterFirstPurchase(u.credit, promoData.amountCharge, amount)
    const creditHistoryAmount = helpers.calcCreditHistoryFistPurchase(promoData.amountCharge, amount)

    const [,,,i] = await Promise.all([
      updateUserCredit(userId, userCredit, t),
      recordUserCreditChange(p.id, creditHistoryAmount, userId, t),
      purchasePayFirstInstallment(p.id, totalInstallments, userId, reqBody.payeeAgent, t, pm.id, adminFeeAmount, promoData),
      addSucceededFirstInstallment(chargeAmount,
        {purchaseId: p.id, transaction: t, paymentMethodId: pm.id, userId: userId}),
    ])
    const tranId = await charge(pm, chargeAmount, `First installment for purchase: ${p.id}`, u.id, p.id, i.id)
    const body = buildNotifyBody(reqBody, p, m, '', 0)
    notifyMerchant(p, body)
    await t.commit()
    saveInstallmentTranId(i.id, tranId)
    sendEmail(u.email, userCredit, chargeAmount, p.id)
    updatePromotion(promoData, {id: userId})
    accountingSyncPurchase(p.id, i)
    return {success: true, returnUrl: helpers.addQueriesToUrl(returnUrl, body)}
  } catch (e) {
    await t.rollback()
    Log.error('USER_CONFIRM_PURCHASE ERROR', e, userId)
    const body = buildNotifyBody(reqBody, currentPurchase, m, _.get(e, 'message', ''), _.get(e, 'status'))
    notifyMerchant(currentPurchase, body)
    handlePurchaseError(merReqId, _.get(e, 'status'), _.get(e, 'message', ''), 1)
    const errorMess = httpError.isHttpError(e) ? e.message : errorTags.SERVER_ERROR
    const errorStatus = httpError.isHttpError(e) ? e.status : 500
    return {success: false, returnUrl: helpers.addQueriesToUrl(returnUrl, body), errors: [{message: errorMess}], code: errorStatus}
  }
}