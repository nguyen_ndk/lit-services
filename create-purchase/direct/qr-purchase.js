const uuid = require('uuid')
const axios = require('axios')
const moment = require('moment')
const _ = require('lodash')
const axiosRetry = require('axios-retry')
axiosRetry(axios, { retryDelay: axiosRetry.exponentialDelay})

const {facade: {DBFacade, Log}, helpers} = require('lit-utils')
const {
  UserRepo,
  PurchaseRepo,
  PaymentMethodRepo, MerchantRepo
} = require('lit-repositories')
const {
  PURCHASE,
  PURCHASE_PAYMENT_STATUS,
  INSTALLMENT,
} = require('lit-constants')
const {Email} = require('lit-notifications')

const recordUserCreditChange = require('../../record-user-credit-change')
const addSucceededFirstInstallment = require('../../add-succeeded-first-installment')
const charge = require('../../payment/direct/charge')
const updateUserCredit = require('../../update-user-credit')
const saveInstallmentTranId = require('../../save-installment-tranid')
const notifyMerchant = require('../../notify-merchant')
const accountingSyncPurchase = require('../../accounting/sync-purchase')
const createPurchase = async (reqBody, m, context) => {
  const saveData = {
    merchantId: m.id,
    notifyUrl: _.get(m, 'notifyUrl'),
    originalAmount: reqBody.originalAmount,
    promotionId: reqBody.promotionId,
    amountDiscount: reqBody.amountDiscount,
    amount: reqBody.amount,
    userId: context.userId,
    totalInstallments: context.totalInstallments,
    transactionId: uuid.v4(),
    source: PURCHASE.SOURCE.QR,
    payeeAgent: PURCHASE.PAYEE_AGENT.QR,
    paymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
    payFirstInstallmentAt: moment().format(),
    paidInstallments: 1,
    dueInstallments: 1,
    description: reqBody.description,
    dueGap: INSTALLMENT.DUE_GAP,
    paymentMethodId: context.pmId,
    adminFeeAmount: context.adminFeeAmount
  }
  return await PurchaseRepo.create(saveData, {transaction: context.transaction})
}

const sendEmail = (email, credit, deducted, p, m) => {
  Email.sendNewPurchase(email, credit, deducted, p, m)
}

const _validate = (m, u, pm, amount) => {
  MerchantRepo.verifyMerchantValidity(m)
  PaymentMethodRepo.verifyPaymentMethod(pm, u)
  UserRepo.verifyUserValidity(u)
  UserRepo.checkCreditSufficiency(u, amount)
}


module.exports = async (reqUser, reqBody) => {
  Log.info('USER_QR_PURCHASE START', {userId: reqUser.id}, reqBody)
  const m = await MerchantRepo.findByClientID(reqBody.clientId)
  const t = await DBFacade.transaction()
  let p, userCredit
  try {
    const [u, pm] = await Promise.all([
      UserRepo.selectForUpdate(reqUser.id, t),
      PaymentMethodRepo.findById(reqBody.paymentMethodId, {transaction: t})
    ])
    _validate(m, u, pm, reqBody.amount)
    var {amount, chargeAmount, totalInstallments, adminFeeAmount} =
      MerchantRepo.calcInstallment(reqBody.amount, m)
    p = await createPurchase(reqBody, m,
      {userId: reqUser.id, transaction: t, totalInstallments, pmId: pm.id, adminFeeAmount})
    userCredit = helpers.userCreditAfterFirstPurchase(u.credit, p.amount, amount)
    const creditHistoryAmount = helpers.calcCreditHistoryFistPurchase(p.amount, amount)
    const [,,i] = await Promise.all([
      updateUserCredit(reqUser.id, userCredit, t),
      recordUserCreditChange(p.id, creditHistoryAmount, reqUser.id, t),
      addSucceededFirstInstallment(chargeAmount,
        {purchaseId: p.id, transaction: t, paymentMethodId: pm.id, userId: p.userId}),
    ])
    const tranId = await charge(pm, chargeAmount, `First installment for purchase: ${p.id}`, u.id, p.id, i.id)
    await t.commit()
    saveInstallmentTranId(i.id, tranId)
    accountingSyncPurchase(p.id, i)
  } catch (e) {
    Log.error('USER_QR_PURCHASE ERROR', e, {userId: reqUser.id})
    await t.rollback()
    throw e
  }

  sendEmail(reqUser.email, userCredit, chargeAmount, p, m)
  if (p.notifyUrl) {
    notifyMerchant(p, m)
  }
  return p
}