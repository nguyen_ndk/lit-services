const httpError = require('http-errors')
const axios = require('axios')
const axiosRetry = require('axios-retry')
const { PromotionRepo} = require('lit-repositories')
axiosRetry(axios, { retryDelay: axiosRetry.exponentialDelay})

const {
  PurchaseRepo,
  UserRepo,
  PaymentMethodRepo,
  MerchantRepo,
} = require('lit-repositories')
const {helpers} = require('lit-utils')
const {
  PURCHASE_PAYMENT_STATUS,
  errorTags,
  PAYMENT_GATEWAY_REQUEST,
  INSTALLMENT
} = require('lit-constants')

const calcChargeData = require('../../payment/3d-sercure/calc-charge-data')
const validationService = require('../../validation')
const verifyPurchaseInValidState = p => {
  if (p.paymentStatus !== PURCHASE_PAYMENT_STATUS.INITIALIZED)
    throw httpError(403, errorTags.PURCHASE_ALREADY_PROCESSED)
}


const checkPurchaseValidity = p => {
  if (!p) throw httpError(404, errorTags.PURCHASE_NOT_FOUND)
  verifyPurchaseInValidState(p)
}

const _validate = (m, u, pm, p) => {
  MerchantRepo.verifyMerchantValidity(m)
  PaymentMethodRepo.verifyPaymentMethod(pm, u)
  UserRepo.verifyUserValidity(u)
  UserRepo.checkCreditSufficiency(u, p.amount)
  checkPurchaseValidity(p)
  validationService.mustBeAtm(pm)
}

const updatePurchase = async (req, purchaseId, context) => {
  const saveData = {
    originalAmount: req.originalAmount,
    promotionId: req.promotionId,
    amountDiscount: req.amountDiscount,
    amount: req.amount,
    userId: req.user.id,
    totalInstallments: context.totalInstallments,
    payeeAgent: req.payeeAgent,
    paidInstallments: 0,
    dueInstallments: 1,
    description: req.description,
    dueGap: INSTALLMENT.DUE_GAP,
    paymentMethodId: context.pmId,
    adminFeeAmount: context.adminFeeAmount
  }
  return await PurchaseRepo.update(purchaseId, saveData)
}

module.exports = async (req) => {
  const merReqId = helpers.genMerReqId(req)
  const [m, pm, p] = await Promise.all([
    MerchantRepo.findByClientID(req.clientId),
    PaymentMethodRepo.findById(req.paymentMethodId),
    PurchaseRepo.findByMerchantRequestId(merReqId)
  ])
  PaymentMethodRepo.verifyPaymentMethod(pm, req.user)
  _validate(m, req.user, pm, p)
  checkPurchaseValidity(p)
  let promoData = await PromotionRepo.verifyPurchase({userId: req.user.id, amount: p.originalAmount}, req.voucherCode)

  var {chargeAmount, totalInstallments, adminFeeAmount} =
    MerchantRepo.calcInstallment(promoData.amountCharge, m)
  req.amount = promoData.amountCharge
  req.promotionId = promoData.promotionId
  req.amountDiscount = promoData.discountAmount
  await Promise.all([
    updatePurchase(req, p.id, {totalInstallments, pmId: pm.id, adminFeeAmount}),
  ])
  const reqType = PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_NEW_3D_SECURE
  req.purchaseId = p.id
  let res = await calcChargeData(req, chargeAmount, pm, reqType)
  return res
}