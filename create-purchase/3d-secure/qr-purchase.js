const _ = require('lodash')

const {
  UserRepo,
  PurchaseRepo,
  PromotionRepo,
  PaymentMethodRepo, MerchantRepo
} = require('lit-repositories')
const {
  PURCHASE,
  PURCHASE_PAYMENT_STATUS,
  INSTALLMENT,
  PAYMENT_GATEWAY_REQUEST,
} = require('lit-constants')
const calcChargeData = require('../../payment/3d-sercure/calc-charge-data')
const validationService = require('../../validation')
const createPurchase = async (req, m, context) => {
  const saveData = {
    merchantId: m.id,
    notifyUrl: _.get(m, 'notifyUrl'),
    originalAmount: req.originalAmount,
    promotionId: req.promotionId,
    amountDiscount: req.amountDiscount,
    amount: req.amount,
    userId: req.user.id,
    totalInstallments: context.totalInstallments,
    source: PURCHASE.SOURCE.QR,
    payeeAgent: PURCHASE.PAYEE_AGENT.QR,
    paymentStatus: PURCHASE_PAYMENT_STATUS.INITIALIZED,
    paidInstallments: 0,
    dueInstallments: 1,
    description: req.description,
    dueGap: INSTALLMENT.DUE_GAP,
    paymentMethodId: context.pmId,
    adminFeeAmount: context.adminFeeAmount
  }
  return await PurchaseRepo.create(saveData)
}


const _validate = (m, u, pm, amount) => {
  MerchantRepo.verifyMerchantValidity(m)
  PaymentMethodRepo.verifyPaymentMethod(pm, u)
  UserRepo.verifyUserValidity(u)
  UserRepo.checkCreditSufficiency(u, amount)
  validationService.mustBeAtm(pm)
}


module.exports = async (req) => {
  const m = await MerchantRepo.findByClientID(req.clientId)
  const pm = await PaymentMethodRepo.findById(req.paymentMethodId)
  let promoData = await PromotionRepo.verifyPurchase({userId: req.user.id, amount: req.originalAmount}, req.voucherCode)
  _validate(m, req.user, pm, req.amount)
  const {chargeAmount, totalInstallments, adminFeeAmount} =
    MerchantRepo.calcInstallment(promoData.amountCharge, m)
  req.amount = promoData.amountCharge
  req.promotionId = promoData.promotionId
  req.amountDiscount = promoData.discountAmount
  const [p] = await Promise.all([
    createPurchase(req, m, {totalInstallments, pmId: pm.id, adminFeeAmount}),
  ])
  const reqType = PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_QR_3D_SECURE
  req.purchaseId = p.id
  const res = await calcChargeData(req, chargeAmount, pm, reqType)
  return res
}