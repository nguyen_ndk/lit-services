const {LateFee, PaymentGatewayRequest} = require('lit-models')

const {PURCHASE_CONFIG} = require('lit-constants')
const {PurchaseLineRepo} = require('lit-repositories')
const {facade: {Log}} = require('lit-utils')

const createTypeLateFee = (lateFee) => {
  const saveData =
    {
      label: (lateFee.feeNumber === 1) ? 'Late fee penalty' : 'Daily late fee',
      type: (lateFee.feeNumber === 1) ? PURCHASE_CONFIG.PURCHASE_LINE_TYPE.LATE_FEE_PENALTY : PURCHASE_CONFIG.PURCHASE_LINE_TYPE.DAILY_LATE_FEE,
      dueDate: lateFee.dueDate,
      amount: lateFee.amount,
      operator: PURCHASE_CONFIG.OPERATOR.DUE,
      isRefundable: false,
      purchaseId: lateFee.purchaseId,
      purchaseInstallmentId: lateFee.purchaseInstallmentId,
      lateFeeId: lateFee.id,
      userId: lateFee.userId
    }

  return PurchaseLineRepo.create(saveData).catch(e => {
    Log.error(`ERROR ADD_PURCHASE_LINES_PAYMENT USER : ${lateFee.userId} LATE_FEE : ${lateFee.id}`, e)
  })
}
//For each payment have to be seperated by purchase ID
const createTypePaymentGatewayRequest = (paymentRequest, options = {}) => {
  const saveData =
    {
      label: 'Overall Payment',
      type: PURCHASE_CONFIG.PURCHASE_LINE_TYPE.PAYMENT,
      amount: options.amountDue,
      operator: PURCHASE_CONFIG.OPERATOR.PAYMENT,
      isRefundable: false,
      userId: paymentRequest.userId,
      paymentGatewayRequestId: paymentRequest.id,
      purchaseId: options.purchaseId
    }

  return PurchaseLineRepo.create(saveData).catch(e => {
    Log.error(`ERROR ADD_PURCHASE_LINES_PAYMENT USER : ${paymentRequest.userId} PAYMENT_REQUEST_ID : ${paymentRequest.id}`, e)
  })
}

module.exports = async (object, options = {}) => {
  // const t = await DBFacade.transaction()

  try {
    if (object instanceof LateFee) {
      return createTypeLateFee(object, options)
    } else if (object instanceof PaymentGatewayRequest){
      return createTypePaymentGatewayRequest(object, options)
    }

    // await t.commit()
  } catch (e) {
    Log.error(`CREATE_PURCHASE_LINE_ERROR UPDATE_DATA purchase: ${object}`, e)

    // await t.rollback()

  }
}