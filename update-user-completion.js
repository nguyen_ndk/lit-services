const {USER} = require('lit-constants')
const {facade: {DBFacade, Log}} = require('lit-utils')
const {UserCompletionRepo} = require('lit-repositories')
const _ = require('lodash')
const {UserRepo} = require('lit-repositories')
const southTelecom = require('./south-telecom')
const {ADMIN_FE_BASE_URL} = require('lit-constants')
const { Sms, Email } = require('lit-notifications')
const approveUserCredit = require('./approve-user-credit')
const _getUpdatedData = (step) => {
  switch (step) {
    case USER.COMPLETION.CARD:
      return 'paymentMethodAdded'
    case USER.COMPLETION.ID:
      return 'kycPassed'
    case USER.COMPLETION.EMAIL:
      return 'emailVerified'
    case USER.COMPLETION.PHONE:
      return 'phoneVerified'

  }
}

const syncCrmUser = (userId) => {
  Log.info(`CREATE_USER_TO_CRM USER ${userId}`)
  UserRepo.findById(userId)
    .then(user => southTelecom.createCustomer(user.id,
      user.firstName+' '+ user.lastName,
      user.phone,
      user.email,
      `${ADMIN_FE_BASE_URL}/users/${user.id}`,
      0)
    )
    .catch(error => {
      Log.error(`CREATE_USER_TO_CRM USER ${userId}`, error)
    })
}
module.exports = async (userId, step) => {
  const field = _getUpdatedData(step)
  const t = await DBFacade.transaction()
  let isActive = false
  try {
    let user = await UserRepo.findById(userId)
    const instance = await UserCompletionRepo.findByUserId(userId, t, true)
    if (_.get(instance, field)) {
      await t.rollback()
      return
    }
    const updatedData = {
      [field]: true,
      userId
    }
    updatedData.completion = _.get(instance, 'completion', 0) + 25
    const promises = []
    if (updatedData.completion === 100) {
      promises.push(UserRepo.activateUser(userId))
      Sms.sendWelcome(user)
      Email.sendWelcome(user)
      isActive = true
    }
    const fn = instance
      ? UserCompletionRepo.update(instance.id, updatedData, {transaction: t, getModel: false})
      : UserCompletionRepo.create(updatedData, {transaction: t})
    promises.push(fn)
    await Promise.all(promises)
    await t.commit()
    if (isActive) {
      syncCrmUser(userId)
      approveUserCredit(userId)
    }
  } catch (e) {
    await t.rollback()
    Log.error(`UPDATE_USER_COMPLETION USER ${userId} STEP ${step}`, e)
  }
}