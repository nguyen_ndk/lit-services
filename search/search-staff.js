const _ = require('lodash')
const { StaffRepo } = require('lit-repositories')
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()
const { DEFAULT_PAGE_LENGTH } = require('lit-constants')

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: (currentPage - 1) * limit,
    limit,
  }

  if (conditions.attributes) options.attributes = conditions.attributes

  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }

      if (typeof filter.value === 'undefined') return

      where[filter.name] = { [filter.method]: filter.value }
    })
    options.where = where
  }

  const staff = await StaffRepo.findAndCountAll(options)

  return {
    totalItems: staff.count,
    items: staff.rows,
    currentPage,
    totalPages: _.ceil(staff.count / limit),
  }
}
