const _ = require('lodash')
const {CategoryRepo} = require('lit-repositories')
const {DEFAULT_PAGE_LENGTH, QUERY_METHOD_MAPPING} = require('lit-constants')

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: ( currentPage - 1) * limit,
    limit,
  }
  // if (conditions.attributes) options.attributes = conditions.attributes
  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, filter => {
      where[filter.name] = {[QUERY_METHOD_MAPPING[filter.method]]: filter.value}
    })
    options.where = where
  }

  if (conditions.orders) {
    const order = []
    _.forEach(conditions.orders, item => {
      order.push([item.field, item.asc ? 'ASC' : 'DESC'])
    })
    options.order = order
  }
  const categories = await CategoryRepo.findAndCountAll(options)
  return {
    totalItems: categories.count,
    items: categories.rows,
    currentPage,
    totalPages: _.ceil(categories.count / limit)
  }
}