const _ = require('lodash')
const { CompanyRepo } = require('lit-repositories')
const { DEFAULT_PAGE_LENGTH } = require('lit-constants')
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: (currentPage - 1) * limit,
    limit,
  }
  // if (conditions.attributes) options.attributes = conditions.attributes
  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }

      if (typeof filter.value === 'undefined') return

      where[filter.name] = { [filter.method]: filter.value }
    })
    options.where = where
  }

  console.log(conditions)

  if (conditions.orders) {
    const order = []
    _.forEach(conditions.orders, (item) => {
      order.push([item.field, item.asc ? 'ASC' : 'DESC'])
    })
    options.order = order
  }

  const companies = await CompanyRepo.findAndCountAll(options)
  console.log(options)
  console.log(conditions)
  console.log('in')
  console.log(companies)

  return {
    totalItems: companies.count,
    items: companies.rows,
    currentPage,
    totalPages: _.ceil(companies.count / limit),
  }
}
