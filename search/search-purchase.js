const _ = require('lodash')

const {
  facade: {DBFacade},
  providers: {DB_PROVIDER},
} = require('lit-utils')
const {col, fn} = require('sequelize')
const {PurchaseRepo} = require('lit-repositories')
const {DEFAULT_PAGE_LENGTH, PURCHASE_CONFIG} = require('lit-constants')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()
const { Address, Merchant, User, PurchaseInstallment, PurchaseLine } = require('lit-models')
const { QUERY_METHOD_MAPPING } = require('lit-constants')

const buildInstallmentQuery = () => {
  return {
    model: PurchaseInstallment,
    include: {
      model: PurchaseLine,
      as: 'PurchaseLineLateFee',
      separate: true,
      attributes: [[fn('sum', col('amount')), 'lateFeeSumAmount'],
        [fn('min', col('due_date')), 'dueDateStart'],
        [fn('max', col('due_date')), 'dueDateEnd'],
      ],
      where: {
        type: {
          [QUERY_METHOD_MAPPING.IN]: [PURCHASE_CONFIG.PURCHASE_LINE_TYPE.LATE_FEE_PENALTY, PURCHASE_CONFIG.PURCHASE_LINE_TYPE.DAILY_LATE_FEE]
        },
        operator: PURCHASE_CONFIG.OPERATOR.DUE
      },
      group: ['purchaseInstallmentId'],
    },
  }
}

const buildQuery = ({filters, attributes, filtersOr}, model) => {
  const query = {
    model: model,
    required: true,
    attributes,
  }

  if (filters) {
    const where = {}
    _.forEach(filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = {[filter.method]: dbConnection.col(filter.value)}
        return
      }
      where[filter.name] = {[filter.method]: filter.value}
    })
    query.where = where
  }

  if (filtersOr) {
    const where = {}
    _.forEach(filtersOr, (filter) => {
      if (filter.lower) {
        where[filter.name] = dbConnection.where(dbConnection.fn('LOWER', dbConnection.col(filter.name)), 'LIKE', '%' + filter.value + '%')
      }
      if (filter.valueType === 'col') {
        where[filter.name] = {[filter.method]: dbConnection.col(filter.value)}
        return
      }
      where[filter.name] = {[filter.method]: filter.value}
    })

    query.where = {[QUERY_METHOD_MAPPING.OR]: where}
  }

  return query
}

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: (currentPage - 1) * limit,
    limit,
  }
  if (conditions.attributes) options.attributes = conditions.attributes

  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = {[filter.method]: dbConnection.col(filter.value)}
        return
      }
      where[filter.name] = {[filter.method]: filter.value}
    })
    options.where = where
  }

  if (conditions.group) options.group = conditions.group
  if (conditions.order) options.order = conditions.order

  if (conditions.includeInstallment) {
    options.distinct = true
    options.include = options.include || []
    options.include.push(buildInstallmentQuery(conditions.installmentFilters))
  }

  if (!_.isEmpty(conditions.users)) {
    options.include = options.include || []
    options.include.push(buildQuery(conditions.users, User))
  }


  if (conditions.includeMerchant) {
    options.include = options.include || []
    options.include.push({
      model: Merchant,
      include: {
        model: Address
      }
    })
  }

  if (conditions.includeUser) {
    options.include = options.include || []
    options.include.push({
      model: User
    })
  }

  if (conditions.orders) {
    const order = []
    _.forEach(conditions.orders, (item) => {
      order.push([item.field, item.asc ? 'ASC' : 'DESC'])
    })
    options.order = order
  }

  const items = await PurchaseRepo.findAndCountAll(options)

  return {
    totalItems: items.count,
    items: items.rows,
    currentPage,
    totalPages: _.ceil(items.count / limit),
  }
}
