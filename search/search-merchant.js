const _ = require('lodash')
const {MerchantRepo} = require('lit-repositories')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()
const {DEFAULT_PAGE_LENGTH} = require('lit-constants')
const {Company, Marketing, MerchantCategory} = require('lit-models')

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: ( currentPage - 1) * limit,
    limit,
    include: []
  }

  if (conditions.includeCompany) {
    options.include.push({
      model: Company
    })
  }

  if (conditions.includeMarketing) {
    options.include.push({
      model: Marketing
    })
  }

  if (conditions.includeMerchantCategory) {
    conditions.includeMerchantCategory.model = MerchantCategory
    options.include.push(conditions.includeMerchantCategory)
  }

  if (conditions.attributes) options.attributes = conditions.attributes

  if (conditions.group) options.group = conditions.group
  if (conditions.order) options.order = conditions.order


  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, filter => {
      
      if (filter.valueType === 'col') {
        where[filter.name] = {[filter.method]: dbConnection.col(filter.value)}
        return
      }

      if (typeof filter.value === 'undefined') return

      where[filter.name] = {[filter.method]: filter.value}
    })
    options.where = where
  }

  const merchants = await MerchantRepo.findAndCountAll(options)


  return {
    totalItems: merchants.count.length,
    items: merchants.rows,
    currentPage,
    totalPages: _.ceil(merchants.count / limit)
  }
}