const _ = require('lodash')

const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils')
const { PurchaseRepo } = require('lit-repositories')
const { DEFAULT_PAGE_LENGTH, QUERY_METHOD_MAPPING } = require('lit-constants')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()
const { PurchaseInstallment, Merchant, User } = require('lit-models')
const buildInstallmentQuery = () => {
  return {
    model: PurchaseInstallment,
  }
}

// const buildMerchantQuery = () => {
//   return {
//     model: Merchant,
//     required: true,
//   }
// }

const buildUserQuery = ({ filters, attributes }) => {
  const query = {
    model: User,
    required: true,
    attributes,
  }
  if (filters) {
    const where = {}
    _.forEach(filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }
      where[filter.name] = { [filter.method]: filter.value }
    })
    query.where = { [QUERY_METHOD_MAPPING.OR]: where }
  }

  return query
}

const buildMerchantQuery = ({ filters, attributes }) => {
  const query = {
    model: Merchant,
    required: true,
    attributes,
  }
  if (filters) {
    const where = {}
    _.forEach(filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }
      where[filter.name] = { [filter.method]: filter.value }
    })
    query.where = where
  }
  return query
}

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: (currentPage - 1) * limit,
    limit,
  }
  if (conditions.attributes) options.attributes = conditions.attributes
  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }
      where[filter.name] = { [filter.method]: filter.value }
    })
    options.where = where
  }
  if (conditions.group) options.group = conditions.group
  if (conditions.order) options.order = conditions.order

  if (conditions.includeInstallment) {
    options.distinct = true
    options.include = options.include || []
    options.include.push(buildInstallmentQuery(conditions.installmentFilters))
  }

  if (!_.isEmpty(conditions.users)) {
    options.include = options.include || []
    options.include.push(buildUserQuery(conditions.users))
  }

  if (!_.isEmpty(conditions.merchant)) {
    options.include = options.include || []
    options.include.push(buildMerchantQuery(conditions.merchant))
  }

  if (conditions.orders) {
    const order = []
    _.forEach(conditions.orders, (item) => {
      order.push([item.field, item.asc ? 'ASC' : 'DESC'])
    })
    options.order = order
  }

  const items = await PurchaseRepo.findAndCountAll(options)

  return {
    totalItems: items.count,
    items: items.rows,
    currentPage,
    totalPages: _.ceil(items.count / limit),
  }
}
