const _ = require('lodash')
const userRepo = require('lit-repositories/user')
const {DEFAULT_PAGE_LENGTH, QUERY_METHOD_MAPPING} = require('lit-constants')

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: ( currentPage - 1) * limit,
    limit,
  }
  if (conditions.attributes) options.attributes = conditions.attributes
  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, filter => {
      where[filter.name] = {[QUERY_METHOD_MAPPING[filter.method]]: filter.value}
    })
    options.where = where
  }

  const users = await userRepo.findAndCountAll(options)
  return {
    totalItems: users.count,
    items: users.rows,
    currentPage,
    totalPages: _.ceil(users.count / limit)
  }
}