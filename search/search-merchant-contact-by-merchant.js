const _ = require('lodash')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()
const {DEFAULT_PAGE_LENGTH} = require('lit-constants')
const {MerchantContact} = require('lit-models')


module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: (currentPage - 1) * limit,
    limit,
  }

  if (conditions.attributes) options.attributes = conditions.attributes

  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, filter => {

      if (filter.valueType === 'col') {
        where[filter.name] = {[filter.method]: dbConnection.col(filter.value)}
        return
      }

      if (typeof filter.value === 'undefined') return

      where[filter.name] = {[filter.method]: filter.value}
    })
    options.where = where
  }


  const model = await MerchantContact.findAndCountAll(options)


  return {
    totalItems: model.count,
    items: model.rows,
    currentPage,
    totalPages: _.ceil(model.count / limit)
  }
}




// const {MerchantContact} = require('../../models/merchant-integration-contact')
// const Base = require('./base')

// const _ = require('lodash')
// const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
// const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()
// const {DEFAULT_PAGE_LENGTH} = require('../../../shares/constants')
// const {MerchantContact} = require('../../models/merchant-integration-contact')
//
// class searchMerchantContactByMerchant{
//   constructor(conditions) {
//     this.model = MerchantContact
//     this.conditions = conditions
//   }
//
//
//   getting()
//   {
//
//     const limit = _.get(this.conditions, 'limit', DEFAULT_PAGE_LENGTH)
//     const currentPage = _.get(this.conditions, 'page', 1)
//     const options = {
//       offset: (currentPage - 1) * limit,
//       limit,
//     }
//
//     if (this.conditions.attributes) options.attributes = this.conditions.attributes
//
//     if (this.conditions.filters) {
//       const where = {}
//       _.forEach(this.conditions.filters, filter => {
//
//         if (filter.valueType === 'col') {
//           where[filter.name] = {[filter.method]: dbConnection.col(filter.value)}
//           return
//         }
//
//         where[filter.name] = {[filter.method]: filter.value}
//       })
//       options.where = where
//     }
//
//
//     const model = this.model.findAndCountAll(options)
//
//     console.log(model)
//
//     return {
//       totalItems: model.count,
//       items: model.rows,
//       currentPage,
//       totalPages: _.ceil(model.count / limit)
//     }
//   }
//
// }
// module.exports = async (conditions) => new searchMerchantContactByMerchant(conditions)