const _ = require('lodash')
const { MerchantStaffRepo } = require('lit-repositories')
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()
const { DEFAULT_PAGE_LENGTH } = require('lit-models')
const { Staff } = require('lit-models')

module.exports = async (conditions) => {
  const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
  const currentPage = _.get(conditions, 'page', 1)
  const options = {
    offset: (currentPage - 1) * limit,
    limit,
  }

  if (conditions.includeStaff) {
    const where = {}

    if (conditions.filtersStaff) {
      _.forEach(conditions.filtersStaff, (filter) => {
        if (filter.valueType === 'col') {
          where[filter.name] = {
            [filter.method]: dbConnection.col(filter.value),
          }
          return
        }

        if (typeof filter.value === 'undefined') return

        where[filter.name] = { [filter.method]: filter.value }
      })
    }

    options.include = [
      {
        model: Staff,
        where,
      },
    ]
  }

  if (conditions.attributes) options.attributes = conditions.attributes

  if (conditions.filters) {
    const where = {}
    _.forEach(conditions.filters, (filter) => {
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }

      where[filter.name] = { [filter.method]: filter.value }
    })
    options.where = where
  }

  const staffMechant = await MerchantStaffRepo.findAndCountAll(options)

  return {
    totalItems: staffMechant.count,
    items: staffMechant.rows,
    currentPage,
    totalPages: _.ceil(staffMechant.count / limit),
  }
}
