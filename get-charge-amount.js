const {PurchaseLineRepo} = require('lit-repositories')

const getDue = (purchaseId) => {
  return PurchaseLineRepo.getDueByPurchaseId(purchaseId)
}
module.exports = {
  getDue,
}