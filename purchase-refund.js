const _ = require('lodash')
const async = require('async')
const httpError = require('http-errors')
const {PurchaseRepo, PurchaseInstallmentRepo, UserRepo, MerchantRepo, PaymentGatewayRequestRepo} = require('lit-repositories')
const {PURCHASE, TRANSACTION_FEE, errorTags, DEFAULT_CURRENCY, INSTALLMENT, PURCHASE_PAYMENT_STATUS, CREDIT_HISTORY_TYPE} = require('lit-constants')
const {facade: {Log, DBFacade}} = require('lit-utils')
const updateUserCredit = require('./update-user-credit')
const recordUserCreditChange = require('./record-user-credit-change')
const refund = require('./payment/refund')
const {Email} = require('lit-notifications')


const validForRefund = (p, req, company) => {
  if (_.includes([
    PURCHASE_PAYMENT_STATUS.REFUNDED_WITH_FAILED_INSTALLMENT,
    PURCHASE_PAYMENT_STATUS.REFUNDED,
    PURCHASE_PAYMENT_STATUS.REFUNDING], p.paymentStatus)) {
    throw httpError(403, errorTags.CANNOT_REFUND_PURCHASE)
  }
  if (_.get(req, 'staff.companyId') !== company.id) throw httpError(403, errorTags.PURCHASE_NOT_BELONG_TO_COMPANY)
}
/**
 * Refund purchase, the process will call payment gateway to refund purchase installment 1 by 1.
 * if all the installments refunded successfully,  purchase status = 'refunded'
 * if all the installments refunded successfully but there are failed installment,  purchase status = 'refunded with failed installment'
 * if not all installment refunded successfully,  purchase status = 'refunding'
 * after process all installment, update purchase status, return credit to user (sum all successfully refunded installment)
 * and save the user credit change
 * @param req.purchaseId: integer
 * @returns {Promise<void>}
 */
module.exports = async (req) => {
  const p = await PurchaseRepo.findById(req.purchaseId, {includeInstallment: true})
  const merchant = await MerchantRepo.findWithCompany(p.merchantId)
  const company = _.get(merchant, 'Company', {})
  validForRefund(p, req, company)
  const list = {}
  let returnCredit = 0
  let index = 0
  let newPurchaseStatus = PURCHASE_PAYMENT_STATUS.REFUNDED
  
  _.forEach(p.PurchaseInstallments, i => {
    index++
    const amount = i.installmentNumber === 1 ? i.amount - TRANSACTION_FEE : i.amount
    console.log('status =====================>', i.status)
    if (i.status !== PURCHASE.INSTALLMENT_STATUS.VALIDATED) {
      newPurchaseStatus = PURCHASE_PAYMENT_STATUS.REFUNDED_WITH_FAILED_INSTALLMENT
      return
    }
    console.log(i.transactionId)
    list[index] = {
      transactionId: i.transactionId,
      amount,
      id: i.id
    }
  })


  await async.eachLimit(list, INSTALLMENT.NUMBER, async (item) => {
    const pgr = await PaymentGatewayRequestRepo.findByTranIdWithPaymentMethod(item.transactionId)
    const ctx = {
      tranId: item.transactionId,
      userId: p.userId,
      gateway: _.get(pgr, 'dataValues.PaymentMethod.gateway'),
      paymentMethodType: _.get(pgr, 'PaymentMethod.type'),
    }
    return refund(item.amount, DEFAULT_CURRENCY, ctx)
      .then((refundTranId) => {
        PurchaseInstallmentRepo.refund(item.id, refundTranId)
      })
      .then(() => {
        console.log('amount =================>', item.amount)
        returnCredit += item.amount
      })
      .catch((e) => {
        newPurchaseStatus = PURCHASE_PAYMENT_STATUS.REFUNDING
        Log.error(`REFUND_ERROR REFUND_INSTALLMENT purchase: ${p.id} installment: ${item.id}`, e)
      })
  })

  const t = await DBFacade.transaction()

  let user
  try {
    user = await UserRepo.selectForUpdate(p.userId, t)
    const merchant = await MerchantRepo.findById(p.merchantId)
    await Promise.all([
      updateUserCredit(p.userId, user.credit + returnCredit, t),
      PurchaseRepo.refund(p, newPurchaseStatus, t),
      recordUserCreditChange(p.id, returnCredit, p.userId, t, CREDIT_HISTORY_TYPE.CREDIT),
    ])
    Email.sendRefundSuccessForUser(user.email, returnCredit, user.credit, p, merchant).catch(e => {
      Log.error(`ERROR SEND EMAIL REFUND SUCCESS FOR USER purchase: ${p.id}`, e)
    })

    await t.commit()
  } catch (e) {
    Email.sendFailedToRefund(user.email, req.purchaseId).catch(e => {
      Log.error(`REFUND_ERROR FAILED_TO_SEND_EMAIL purchase: ${p.id}`, e)
    })
    Log.error(`REFUND_ERROR UPDATE_DATA purchase: ${p.id}`, e)
    await t.rollback()
  }
}