const httpError = require('http-errors')
const _ = require('lodash')
const {errorTags, PURCHASE_PAYMENT_STATUS, USER, MERCHANT} = require('lit-constants')
const {PaymentMethodRepo, PurchaseRepo, UserRepo} = require('lit-repositories')

const pmBelongToUser = (paymentMethod, user) => {
  if (paymentMethod.userId !== user.id)  throw httpError(422, errorTags.NOT_OWN_PAYMENT_METHOD)
}

const notFoundPM = pm => {
  if (!pm)  throw httpError(422, errorTags.NOT_FOUND_PAYMENT_METHOD)
}

const purchaseBelongToUser = (purchase, user) => {
  if (purchase.userId !== user.id) throw httpError(422, errorTags.USER_NOT_OWN_PURCHASE)
}

const isPMPayable = (paymentMethod, user) => {
  notFoundPM(paymentMethod)
  pmBelongToUser(paymentMethod, user)
  pmHasBankToken(paymentMethod)
}

const pmHasBankToken = (paymentMethod) => {
  if (!paymentMethod.bankToken) throw httpError(400, errorTags.INVALID_PAYMENT_METHOD)
}

const mustBeAtm = pm => {
  if (!PaymentMethodRepo.isAtm(pm))  throw httpError(422, errorTags.PAYMENT_METHOD_MUST_BE_ATM)
}

const purchaseUnprocessableState = purchase => {
  if (PurchaseRepo.isCompleted(purchase)) {
    throw httpError(422, errorTags.PURCHASE_COMPLETED)
  }
}

const noRemaining = remaining => {
  if (remaining === 0)  throw httpError(422, errorTags.COMPLETED_PURCHASE)
}

const canStartPurchase = (purchase) => {
  if (!_.includes([PURCHASE_PAYMENT_STATUS.INITIALIZED, PURCHASE_PAYMENT_STATUS.FAILED], purchase.paymentStatus))
    throw httpError(403, errorTags.PURCHASE_ALREADY_PROCESSED)
}

const notFoundPurchase = purchase => {
  if (!purchase || _.isEmpty(purchase)) throw httpError(404, errorTags.PURCHASE_NOT_FOUND)
}


const isUserCreditEnough = (u, amount) => {
  if (u.credit < amount) throw httpError(422, errorTags.INSUFFICIENT_CREDIT)
}

const isUserActive = (user) => {
  if (user.status !== USER.STATUS.ACTIVE) throw httpError(403, errorTags.NOT_ALLOW)
}

const canUserMakeNewPurchase = (user, purchase) => {
  isUserCreditEnough(user, purchase.originalAmount)
  isUserActive(user)
}

const canMerchantReceiveNewPurchase = (merchant) => {
  if (_.isEmpty(merchant)) throw httpError(404, errorTags.MERCHANT_NOT_FOUND)
  if (merchant.status !== MERCHANT.STATUS.ACTIVE) throw httpError(403, errorTags.MERCHANT_INACTIVE)
}

const phoneTaken = async phone => {
  const result = await UserRepo.checkPhoneUsed(phone)
  if (result) {
    throw httpError(422, errorTags.PHONE_TAKEN)
  }
}

const emailTaken = async email => {
  const result = await UserRepo.checkEmailUsed(email)
  if (result) {
    throw httpError(422, errorTags.EMAIL_TAKEN)
  }
}

module.exports = {
  isPMPayable,
  pmBelongToUser,
  canStartPurchase,
  notFoundPM,
  purchaseBelongToUser,
  mustBeAtm,
  noRemaining,
  purchaseUnprocessableState,
  notFoundPurchase,
  isUserCreditEnough,
  isUserActive,
  canUserMakeNewPurchase,
  canMerchantReceiveNewPurchase,
  phoneTaken,
  emailTaken,
}
