const {PurchaseInstallmentRepo} = require('lit-repositories')
const {QUERY_METHOD_MAPPING, PURCHASE, DEFAULT_DATETIME_FORMAT} = require('lit-constants')
const {facade: {Log}} = require('lit-utils')
const moment = require('moment')

//Overall billing didnt go throught so we have to set the status of the purchase installment to soft failed
module.exports = (userId, transactionId, purchaseId, paymentMethodId) => {
  const logTag = `VALIDATE_INSTALLMENTS USER_ID: ${userId}`
  Log.info(`${logTag} START`)
  const options = {
    where: {
      userId,
      purchaseId: purchaseId,
      status: {
        [QUERY_METHOD_MAPPING.IN]: [
          PURCHASE.INSTALLMENT_STATUS.DUE,
          PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED,
          PURCHASE.INSTALLMENT_STATUS.HARD_FAILED,
        ]
      }
    }
  }

  const saveData = {
    status: PURCHASE.INSTALLMENT_STATUS.VALIDATED,
    transactionId: transactionId,
    paidAt: moment().format(DEFAULT_DATETIME_FORMAT),
    paymentMethodId: paymentMethodId
  }
  return PurchaseInstallmentRepo.updateMany(saveData, options)
}