const {PurchaseInstallmentRepo} = require('lit-repositories')
const {QUERY_METHOD_MAPPING, PURCHASE} = require('lit-constants')
const {facade: {Log}} = require('lit-utils')

//Overall billing didnt go throught so we have to set the status of the purchase installment to soft failed
module.exports = (userId) => {
  const logTag = `UPDATE_DUE_INSTALLMENT_TO_FAILED USER_ID: ${userId}`
  Log.info(`${logTag} START`)
  const options = {
    where: {
      userId,
      status: {
        [QUERY_METHOD_MAPPING.EQ]: PURCHASE.INSTALLMENT_STATUS.DUE,
      }
    }
  }

  const saveData = {
    status: PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED,
    failedReason: 'Failed overall billing'
  }
  return PurchaseInstallmentRepo.updateMany(saveData, options)
}