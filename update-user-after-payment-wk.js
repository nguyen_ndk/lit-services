const {UserRepo} = require('lit-repositories')
const recordUserCreditHistory = require('./record-user-credit-change')
module.exports = (userId, purchaseId, newUserCredit, creditHistoryAmount, transaction) => {
  return Promise.all([
    UserRepo.updateCredit(userId, newUserCredit, transaction),
    recordUserCreditHistory(purchaseId, creditHistoryAmount, userId, transaction),
  ])
}