const {PurchaseInstallmentRepo} = require('lit-repositories')
const {facade: {Log}} = require('lit-utils')
const {PURCHASE} = require('lit-constants')

module.exports = (installmentId, transactionId) => {
  PurchaseInstallmentRepo.update(installmentId, {transactionId, status: PURCHASE.INSTALLMENT_STATUS.VALIDATED}).catch(e => {
    Log.error(`SAVE_INSTALLMENT_TRANSACTION_ID installment ${installmentId} transaction ${transactionId}`, e)
  })
}