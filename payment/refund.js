const uuid = require('uuid')
const {facade: {PaymentGatewayFacade, Log}} = require('lit-utils')
const {DEFAULT_CURRENCY, PAYMENT_GATEWAY_REQUEST} = require('lit-constants')
const {PaymentGatewayRequestRepo} = require('lit-repositories')

module.exports = async (amount, currency, ctx) => {
  const req = await PaymentGatewayRequestRepo.findByTransactionId(ctx.tranId)
  ctx.gatewayTranId = ctx.gatewayTranId || req.gatewayTranId
  ctx.refundTranId = uuid.v4()
  let error
  try {
    // TODO remove this, receive the gateway from payment method
    var {gatewayTranId} = await PaymentGatewayFacade.provider(ctx.gateway)
      .refund(amount, DEFAULT_CURRENCY, ctx)
  } catch (e) {
    error = e
  }
  const logData = {
    type: PAYMENT_GATEWAY_REQUEST.TYPE.REFUND,
    gateway: ctx.gateway,
    transactionId: ctx.refundTranId,
    userId: ctx.userId,
    purchaseId: ctx.purchaseId,
    status: error ? PAYMENT_GATEWAY_REQUEST.STATUS.FAILED : PAYMENT_GATEWAY_REQUEST.STATUS.PROCESSED,
    gatewayTranId,
    amount,
  }
  PaymentGatewayRequestRepo.create(logData).catch(error => {
    Log.error('PAYMENT_GATEWAY_CHARGE LOG', {logData}, {error})
  })
  if (error) throw error
  return ctx.refundTranId
}