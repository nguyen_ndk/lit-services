const {PurchaseRepo, PaymentMethodRepo, UserRepo} = require('lit-repositories')
const {PayDynamicQrOtp} = require('../../payment-processor')


module.exports = async (extractedData, paymentGatewayReq) => {
  const purchaseId = paymentGatewayReq.purchaseId
  const p = await PurchaseRepo.findByIdWithMerchant(purchaseId)
  const [pm, user] = await Promise.all([
    PaymentMethodRepo.findById(paymentGatewayReq.paymentMethodId),
    UserRepo.findById(paymentGatewayReq.userId)
  ])

  return PayDynamicQrOtp.webhook(paymentGatewayReq, p, pm.id, user)
}