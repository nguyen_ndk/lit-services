const {facade: {Log}} = require('lit-utils')
const _ = require('lodash')

const {PaymentMethodRepo, UserRepo, PaymentGatewayRequestRepo} = require('lit-repositories')
const {PayDueConsolidateOtp} = require('../../payment-processor')


module.exports = async (extractedData, paymentGatewayReq) => {
  const purchases = PaymentGatewayRequestRepo.findByTranIdWithPurchase(extractedData.transactionId)
  const logTag = `WEBHOOK_PAYMENT_3D_SECURE purchases ${_.reduce(purchases, (txt, p) => `${txt}, ${p.id}`, '')}`
  Log.info(logTag)
  const [pm, user] = await Promise.all([
    PaymentMethodRepo.findById(paymentGatewayReq.paymentMethodId),
    UserRepo.findById(paymentGatewayReq.userId)
  ])

  return PayDueConsolidateOtp.webhook(paymentGatewayReq, purchases, pm.id, user)
}

