const uuid = require('uuid')
const {PaymentGatewayRequestRepo} = require('lit-repositories')
const {facade: {PaymentGatewayFacade}} = require('lit-utils')
const {PAYMENT_GATEWAY_REQUEST, WINDOW_TYPE} = require('lit-constants')
module.exports = async (req, amount, pm, type) => {
  let transactionId = uuid.v4()
  const logData = {
    type,
    gateway: pm.gateway,
    transactionId,
    userId: req.user.id,
    purchaseId: req.purchaseId,
    status: PAYMENT_GATEWAY_REQUEST.STATUS.INIT,
    amount,
    installmentId: req.installmentId,
    paymentMethodId: pm.id,
  }
  await PaymentGatewayRequestRepo.create(logData)
  const ctx = {
    tranId: transactionId,
    userEmail: req.user.email,
    userId: req.user.id,
    ip: req.ip,
    paymentMethodType: pm.type,
    bankToken: pm.bankToken,
    windowType: req.windowType || WINDOW_TYPE.DESKTOP,
    purchaseId: req.purchaseId,
    type
  }
  return await PaymentGatewayFacade.provider(pm.gateway).calcCreateTokenData(amount, ctx)
}