const {facade: {DBFacade, Log}, helpers} = require('lit-utils')
const {PurchaseRepo, PurchaseInstallmentRepo, UserRepo, MerchantRepo, PurchaseLineRepo} = require('lit-repositories')
const { PURCHASE_PAYMENT_STATUS} = require('lit-constants')
const {Email} = require('lit-notifications')
const updatePromotion = require('../../promotion/update-after-purchase')
const notifyMerchant = require('../../notify-merchant')
const updateUserAfterPaymentWk = require('../../update-user-after-payment-wk')
const accountingSyncPurchase = require('../../accounting/sync-purchase')
const sendEmail = (email, credit, totalAmount, p) => Email.sendPurchaseCompleted(email, credit, totalAmount, p)
  .catch(e => {
    Log.error(`WEBHOOK_NEW_PURCHASE_3D_SECURE user ${email}`, e)
  })

const _notify = (p) => {
  MerchantRepo.findById(p.merchantId)
    .then(m => {notifyMerchant(p, m)})
}
module.exports = async (extractedData, paymentGatewayReq) => {
  const purchaseId = paymentGatewayReq.purchaseId
  const pmId = paymentGatewayReq.paymentMethodId
  const tranId = extractedData.transactionId
  const firstInstallmentAmount = paymentGatewayReq.amount
  const logTag = `WEBHOOK_NEW_PURCHASE_3D_SECURE purchase ${purchaseId}`
  const p = await PurchaseRepo.findById(purchaseId)
  if (p.paymentStatus !== PURCHASE_PAYMENT_STATUS.INITIALIZED) {
    Log.error(`${logTag} PURCHASE_PROCESSED`)
    return
  }
  const totalAmount = p.amount
  const {amount: baseInstallmentAmount} = helpers.calcInstallment(totalAmount, p.totalInstallments)
  const t = await DBFacade.transaction()
  try {
    const u = await UserRepo.selectForUpdate(p.userId, t)
    const user = {
      id: p.userId
    }
    const promoData = {
      discountAmount: p.amountDiscount,
      promotionId: p.promotionId,
      isApprove: p.promotionId !== 0
    }
    // firstInstallmentAmount contain installment amount  + admin fee -> cannot be used for user credit
    const newUserCredit = helpers.userCreditAfterFirstPurchase(u.credit, totalAmount, baseInstallmentAmount)
    const creditHistoryAmount = helpers.calcCreditHistoryFistPurchase(totalAmount, baseInstallmentAmount)
    const [, i] = await Promise.all([
      PurchaseRepo.markInProgress(p, tranId, t),
      PurchaseInstallmentRepo.addSuccessInstallment(p, firstInstallmentAmount, pmId, tranId, t),
      updateUserAfterPaymentWk(u.id, purchaseId, newUserCredit, creditHistoryAmount, t),
      PurchaseLineRepo.addFirstPayment(p.id, p.userId, baseInstallmentAmount,{transaction: t})
    ])
    await t.commit()
    updatePromotion(promoData, user)
    accountingSyncPurchase(p, i)
    sendEmail(u.email, newUserCredit, firstInstallmentAmount, p)
    if (p.notifyUrl) {
      _notify(p)
    }
  } catch (e) {
    await t.rollback()
    throw e
  }
}

