const uuid = require('uuid')
const _ = require('lodash')
const {PaymentGatewayRequestRepo, PgReqToPurchaseRepo} = require('lit-repositories')
const {facade: {PaymentGatewayFacade, DBFacade}} = require('lit-utils')
const {PAYMENT_GATEWAY_REQUEST, WINDOW_TYPE} = require('lit-constants')
module.exports = async (req, amount, pm) => {
  let transactionId = uuid.v4()
  const transaction = await DBFacade.transaction()
  try {
    const pgr = await PaymentGatewayRequestRepo.create({
      type: PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE,
      gateway: pm.gateway,
      transactionId,
      userId: req.user.id,
      purchaseId: req.purchaseId,
      status: PAYMENT_GATEWAY_REQUEST.STATUS.INIT,
      amount,
      installmentId: req.installmentId,
      paymentMethodId: pm.id,
      installmentNumber: req.installmentNumber,
    }, {transaction})
    await PgReqToPurchaseRepo.bulkCreate(_.map(req.purchaseIds, id => ({purchaseId: id, PgReqId: pgr.id})), {transaction})
    await transaction.commit()
  } catch (e) {
    await transaction.rollback()
    throw e
  }

  const ctx = {
    tranId: transactionId,
    userEmail: req.user.email,
    userId: req.user.id,
    ip: req.ip,
    paymentMethodType: pm.type,
    bankToken: pm.bankToken,
    windowType: req.windowType || WINDOW_TYPE.DESKTOP,
    purchaseId: req.purchaseId,
  }
  return await PaymentGatewayFacade.provider(pm.gateway).getMultiPurchasePaymentFormData(amount, ctx)
}