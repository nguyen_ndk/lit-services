const {facade: {Log, PaymentGatewayFacade}, providers: {PAYMENT_GATEWAY_PROVIDER}} = require('lit-utils')
const {errorTags, PAYMENT_GATEWAY_REQUEST} = require('lit-constants')
const completePaymentGatewayRequest = require('../complete-payment-request-request')
const httpError = require('http-errors')

module.exports = (logTag, processor) => async (req, body) => {
  Log.info(logTag, body)
  let extractedData
  try {
    extractedData = PaymentGatewayFacade.provider(PAYMENT_GATEWAY_PROVIDER.EPAY).extractPayloadData(body)
    if (!req || req.status !== PAYMENT_GATEWAY_REQUEST.STATUS.INIT) {
      throw httpError(403, errorTags.PROCESSED_ENTITY)
    }
    const result = await processor(extractedData, req)
    completePaymentGatewayRequest(
      req,
      PAYMENT_GATEWAY_REQUEST.STATUS.PROCESSED,
      extractedData.gatewayTranId,
      result
    ).catch(e => {
      Log.error(`${logTag} UPDATE_TRANSACTION`, e, body)
    })
  } catch (e) {
    Log.error(logTag, e, body)
    completePaymentGatewayRequest(
      req,
      PAYMENT_GATEWAY_REQUEST.STATUS.FAILED,
      extractedData.gatewayTranId
    ).catch(e => {
      Log.error(`${logTag} UPDATE_TRANSACTION`, e, body)
    })
    throw e
  }

}