const {facade: {Log}} = require('lit-utils')
const {PaymentGatewayRequestRepo, PurchaseRepo} = require('lit-repositories')
const {PAYMENT_GATEWAY_REQUEST} = require('lit-constants')

module.exports = (req) => {
  const saveData = {
    retryCount: req.retryCount + 1
  }
  if (saveData.retryCount === PAYMENT_GATEWAY_REQUEST.RETRY_MAX)
    saveData.status = PAYMENT_GATEWAY_REQUEST.STATUS.ABORTED
  PurchaseRepo.markActive(req.purchaseId).catch(e => {
    Log.error(`INQUIRY_TRANSACTION MARK_PURCHASE_ACTIVE ${req.purchaseId}`, e)
  })
  PaymentGatewayRequestRepo.update(req.id, saveData)
    .catch(e => {
      Log.error(`INQUIRY_TRANSACTION INCREASE_RETRY TRANSACTION ${req.transactionId}`, e)
    })
}