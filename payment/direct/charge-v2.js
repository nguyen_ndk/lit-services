const uuid = require('uuid')
const {facade: {PaymentGatewayFacade, Log}} = require('lit-utils')
const {DEFAULT_CURRENCY, PAYMENT_GATEWAY_REQUEST} = require('lit-constants')
const {PaymentGatewayRequestRepo} = require('lit-repositories')
const accountingSyncPaymentGatewayFee = require('../../accounting/sync-payment-gateway-fee')
module.exports = async (req, amount, pm, desc) => {
  const context = {
    tranId: uuid.v4(),
    batchTranId: uuid.v4(),
    userId: req.user.id
  }
  let error
  try {
    var {gatewayTranId} = await PaymentGatewayFacade.provider(pm.gateway)
      .charge(pm.bankToken, amount, DEFAULT_CURRENCY, desc, context)
  } catch (e) {
    error = e
  }
  //TODO remove type
  const logData = {
    type: PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE,
    gateway: pm.gateway,
    transactionId: context.tranId,
    batchTranId: context.batchTranId,
    userId: req.user.id,
    status: error ? PAYMENT_GATEWAY_REQUEST.STATUS.FAILED : PAYMENT_GATEWAY_REQUEST.STATUS.PROCESSED,
    gatewayTranId,
    amount,
    paymentMethodId: pm.id,
    purchaseId: req.purchaseId,
  }
  PaymentGatewayRequestRepo.create(logData).catch(error => {
    Log.error('PAYMENT_GATEWAY_CHARGE LOG', {logData}, {error})
  })
  if (error) {
    throw error
  }
  accountingSyncPaymentGatewayFee(amount, pm, req.user.id, desc)
  return context.tranId
}