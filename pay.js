const httpError = require('http-errors')
const {PurchaseRepo, PaymentMethodRepo} = require('lit-repositories')
const {errorTags, PAYMENT_METHOD, BLOCK_PAYMENT_FEATURE} = require('lit-constants')
const processors = require('./payment-processor')

const _getProcessor = (paymentMethodType) => {
  switch (paymentMethodType) {
    case PAYMENT_METHOD.TYPE.ATM:
      if (BLOCK_PAYMENT_FEATURE.BLOCK_NAPAS_PAYMENT) {
        throw httpError(403, errorTags.BLOCK_NAPAS_PAYMENT)
      }
      return processors.PayOtp
    case PAYMENT_METHOD.TYPE.CREDIT_CARD:
    case PAYMENT_METHOD.TYPE.DEBIT_CARD:
      if (BLOCK_PAYMENT_FEATURE.BLOCK_CREDIT_PAYMENT) {
        throw httpError(403, errorTags.BLOCK_CREDIT_PAYMENT)
      }
      return processors.PayDirect
    default:
      throw httpError(400, errorTags.INVALID_PAYMENT_TYPE)
  }
}

module.exports = async (req) => {
  const [p, pm] = await Promise.all([
    PurchaseRepo.findById(req.purchaseId),
    PaymentMethodRepo.findById(req.paymentMethodId),
  ])
  const processor = _getProcessor(pm.type)
  processor.validate(pm, p, req.user)

  const chargeAmount = await processor.getAmount(req.installmentNumber, p)
  if (chargeAmount === 0) {
    throw httpError(422, errorTags.COMPLETED_PURCHASE)
  }
  const desc = await processor.getDesc(p.id, chargeAmount, req.installmentNumber)
  const result = await processor.processPayment(req, chargeAmount, pm, desc)
  return processor.update(result, p, pm.id, chargeAmount, req)
}