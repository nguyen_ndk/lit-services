const {facade: {DBFacade, Log}} = require('lit-utils')
const {PurchaseInstallmentRepo, LateFeeRepo} = require('lit-repositories')
const {LATE_FEE, DEFAULT_DATETIME_FORMAT, QUERY_METHOD_MAPPING, PURCHASE} = require('lit-constants')
const moment = require('moment')
const {LateFee, Purchase, User} = require('lit-models')
const {Email} = require('lit-notifications')

const _ = require('lodash')
const createPurchaseLine = require('../purchase-line/create-purchase-line')
const async = require('async')

/*
 GET LIST OF LATE PAYMENT ON PURCHASE INSTALLMENT
 */
const getLatePurchaseInstallement = async (deadlineHours) => {
  try {

    const conditions =
      {
        group: ['id'],
        include: [{
          model: Purchase,
          include: [{
            model: User,
          }]
        },
        {
          model: LateFee,
          separate: true,
          order: [['feeNumber', 'DESC']] //Very Important to get the last late_fee count,
        }],
        where: {
          status:{
            [QUERY_METHOD_MAPPING.IN]: [
              PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED,
              PURCHASE.INSTALLMENT_STATUS.HARD_FAILED,
            ]
          }, //PAYMENT FAILED
          dueDate: {[QUERY_METHOD_MAPPING.LT]: moment().subtract(deadlineHours, 'hours').format(DEFAULT_DATETIME_FORMAT)},//DATETIME WHEN THE LATE FEE SHOULD START
          installmentNumber: {[QUERY_METHOD_MAPPING.GT]: 1} //STARTING ON INSTALLMENT NUMBER 2 ONLY
        }
      }

    const purchaseInstallments = await PurchaseInstallmentRepo.findAll(conditions)

    Log.info('GET_LATE_PURCHASE_INSTALLMENT')

    return purchaseInstallments
  } catch (e) {
    Log.error('GET_LATE_PURCHASE_INSTALLMENT', e)
  }
}


/*
  GET THE CONFIG (FEE AMOUNT) OF THE PURCHASE
 */
const getConfigLine = (purchaseInstallement) => {
  const purchaseAmount = purchaseInstallement.amount
  var result = false
  _.find(LATE_FEE.CONFIG, (LFC) => {
    if (purchaseAmount >= LFC[LATE_FEE.CONFIG_KEY['purchaseAmountMin']] && purchaseAmount <= LFC[LATE_FEE.CONFIG_KEY['purchaseAmountMax']]) {
      return result = LFC
    }
  })

  return result
}

// feeAmount, feeNumber, LFC
const saveLateFee = async (purchaseInstallment, options) => {
  const t = await DBFacade.transaction()

  try {
    var purchaseAmount = purchaseInstallment.amount
    var constants = LATE_FEE.CONFIG_KEY
    constants.values = options.LFC

    //We dont count the first late flat fee penalty as a 20% per year max(by the law)
    if (options.feeNumber === 1)
    {
      options.percentage = 0
      options.cumulativePercentage = 0
      options.cumulativeAmount = 0
    } else {
      options.percentage = getPercentage(options.feeAmount, purchaseAmount)
      options.cumulativePercentage = getPercentage(options.cumulativeAmount, purchaseAmount)
    }

    const saveData = {
      purchaseId: purchaseInstallment.Purchase.id,
      purchaseInstallmentId: purchaseInstallment.id,
      userId: purchaseInstallment.Purchase.userId,
      amount: options.feeAmount,
      percentage: options.percentage,
      dueDate: moment().format(DEFAULT_DATETIME_FORMAT),
      feeNumber: options.feeNumber,
      lateDaysFrequency: options.LFC[LATE_FEE.CONFIG_KEY['lateDaysFrequency']],
      constants: JSON.stringify(constants),
      cumulativeAmount: options.cumulativeAmount,
      cumulativePercentage: options.cumulativePercentage
    }

    const lateFee = await LateFeeRepo.create(saveData)

    await createPurchaseLine(lateFee)

    await t.commit()
    Log.info('SAVE_LATE_FEE')

    //Send email on first late fee
    if (options.feeNumber === 1)
    {
      Email.sendLateFee(purchaseInstallment.Purchase.User, lateFee)
    }


    return lateFee
  } catch (e) {
    Log.error('SAVE_LATE_FEE_ERROR ', e)
    await t.rollback()
  }
}

const isAfterExpectedDueDate = (expectedDueDate, dueDate) => {
  return expectedDueDate.isAfter(dueDate)
}

const isPercentageLimitReached = (cumulativePercentage, maxPercentagePurchaseFeeAmount) => {
  if (parseFloat(cumulativePercentage) < parseFloat(maxPercentagePurchaseFeeAmount)) {
    return false
  }

  return true
}

const getPercentage = (amount, totalAmount) => {
  return _.round((parseFloat(amount) / parseFloat(totalAmount) * 100), 10)
}


module.exports = async (mode) => {
  //Get Late Purchase Installment with failed payment
  const purchaseInstallments = await getLatePurchaseInstallement(LATE_FEE.DEADLINE_HOURS)


  //Foreach Installment failed
  await async.eachLimit(purchaseInstallments, 1, async (purchaseInstallment) => {

    //Late Fee Config per Installment
    var LFC = getConfigLine(purchaseInstallment)

    //Get Existing late fees
    var existingLateFees = purchaseInstallment.LateFees


    // console.log(LFC)
    var firstFee = true
    //IF EXISTING LATE FEE = NOT FIRST TIME
    if (existingLateFees.length > 0) firstFee = false

    //Total amount of the purchase
    var purchaseAmount = purchaseInstallment.amount

    //Fee amount for late fee
    var flatFeeAmount = LFC[LATE_FEE.CONFIG_KEY['flatFeeAmount']]
    var feeNumber = 1

    //VERY FIRST LATE FEE FLAT FEE
    if (firstFee) {
      saveLateFee(purchaseInstallment,
        {
          feeAmount: flatFeeAmount,
          cumulativeAmount: flatFeeAmount,
          feeNumber: feeNumber,
          LFC: LFC,
        })

      return true
    }

    var existingCumulativePercentage = existingLateFees[0].cumulativePercentage
    var maxPercentagePurchaseInstallmentFeeAmount = LFC[LATE_FEE.CONFIG_KEY['maxPercentagePurchaseInstallmentFeeAmount']]

    
    //If its time to apply late feee
    if ((mode === 'nolimit' && !isPercentageLimitReached(existingCumulativePercentage, maxPercentagePurchaseInstallmentFeeAmount)) || (
      isAfterExpectedDueDate(moment().subtract(existingLateFees[0].lateDaysFrequency, 'days'), existingLateFees[0].dueDate) &&
      //If the late fee amount didnt reach the limit
      !isPercentageLimitReached(existingCumulativePercentage, maxPercentagePurchaseInstallmentFeeAmount))) {

      var percentageFeeAmount = LFC[LATE_FEE.CONFIG_KEY['percentageFeeAmount']]
      var cumulativePercentage = _.round((parseFloat(existingCumulativePercentage) + percentageFeeAmount), 10)
      var feeAmount = _.round(purchaseAmount * (percentageFeeAmount / 100))

      //Last percentage
      if (isPercentageLimitReached(cumulativePercentage, maxPercentagePurchaseInstallmentFeeAmount)) {
        const percentageLeft = _.round((parseFloat(maxPercentagePurchaseInstallmentFeeAmount) - parseFloat(existingCumulativePercentage)), 10)

        if (percentageLeft > 0.00) {
          feeAmount = _.round(purchaseAmount * (percentageLeft / 100))
        }
      }

      var cumulativeAmount = existingLateFees[0].cumulativeAmount + feeAmount

      saveLateFee(purchaseInstallment,
        {
          feeAmount: feeAmount,
          cumulativeAmount: cumulativeAmount,
          feeNumber: ++existingLateFees[0].feeNumber,
          LFC: LFC
        })

      return true
    }
  })

  return true
}