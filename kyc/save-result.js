const {KycResultRepo, KalapaResponseRepo} = require('lit-repositories')
const {facade: {DBFacade, Log}, providers: {KYC_PROVIDER}} = require('lit-utils')

const _getRepoByProvider = (provider) => {
  switch (provider) {
    case KYC_PROVIDER.KALAPA:
    default:
      return KalapaResponseRepo
  }
}
module.exports = async (kycResultData, responseId, provider) => {
  const t = await DBFacade.transaction()
  try {
    const kycResult = await KycResultRepo.create(kycResultData, {transaction: t})
    const repo = _getRepoByProvider(provider)
    await repo.update(responseId, {kycResultId: kycResult.id}, {transaction: t})
    await t.commit()
    return kycResult
  } catch (e) {
    await t.rollback()
    Log.error(`SAVE_KYC_RESULT USER ${kycResultData.userId}`, kycResultData, responseId, e)
  }
}