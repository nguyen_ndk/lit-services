const {KalapaResponseRepo} = require('lit-repositories')
const {providers: {KYC_PROVIDER}} = require('lit-utils')

const _getRepoByProvider = (provider) => {
  switch (provider) {
    case KYC_PROVIDER.KALAPA:
    default:
      return KalapaResponseRepo
  }
}

const update = (data, provider, id) => {
  const repo = _getRepoByProvider(provider)
  return repo.update(id, data, {getModel: false})
}

const add = (data, provider, userId) => {
  const repo = _getRepoByProvider(provider)
  return repo.create({...data, userId})
}
module.exports  = {
  update,
  add
}