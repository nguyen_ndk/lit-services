const _ = require('lodash')
const httpError = require('http-errors')
const {KYC, STORAGE} = require('lit-constants')
const {providers: {KYC_PROVIDER}, facade: {FileFacade}} = require('lit-utils')
const getUserInfoById = require('./get-user-info-by-id')
const credit = require('./credit')
const score = require('./score')
const antifraud = require('./antifraud')
const saveResult = require('../save-result')
const moment = require('moment')

const _phrase1 = async (idPhoto, selfie, userId, uuid) => {
  const idRes = await getUserInfoById(idPhoto, selfie, userId, uuid)
  if (!idRes.passed) return idRes
  const antifraudRes = await antifraud(idRes.id, userId, idRes.resId, uuid)
  if (!antifraudRes.passed) return antifraudRes
  const creditRes = await credit(idRes.id, userId, idRes.resId, uuid)
  if (!creditRes.passed) return creditRes
  return {
    passed: true,
    creditData: creditRes.data,
    userData: idRes.userData,
    resId: idRes.resId,
    id: idRes.id
  }
}

const _phrase2 = async (id, userId, resId, uuid)=> {
  const scoreRes = await score(id, userId, resId, uuid)
  return {
    passed: scoreRes.passed,
    scoreData: scoreRes.data,
    errorMsg: scoreRes.errorMsg
  }
}

const _getScore = (hasLatePayment, isCurrentEmployed) => {
  let userWeight = 0
  if (isCurrentEmployed) {
    userWeight += KYC.WEIGHT.IS_CURRENT_EMPLOYED
  }
  if (!hasLatePayment) {
    userWeight += KYC.WEIGHT.HAS_LATE_PAYMENT
  }
  return (userWeight / KYC.WEIGHT.TOTAL) * 100
}

const _getKycResult = async (idPhoto, selfie, userId, uuid) => {
  const phrase1Res = await _phrase1(idPhoto, selfie, userId, uuid)
  if (!phrase1Res.passed) {
    return {
      kycResultData: {
        phraseOneStatus: KYC.STATUS.FAILED,
        phraseTwoStatus: KYC.STATUS.FAILED,
        score: 0
      },
      errorMsg: phrase1Res.errorMsg,
      resId: phrase1Res.resId
    }
  }
  const phrase2Res = await _phrase2(phrase1Res.id, userId, phrase1Res.resId, uuid)
  if (!phrase2Res.passed) {
    return {
      kycResultData: {
        phraseOneStatus: KYC.STATUS.PASSED,
        phraseTwoStatus: KYC.STATUS.FAILED,
        score: 0
      },
      errorMsg: phrase2Res.errorMsg,
      resId: phrase1Res.resId
    }
  }
  const hasLatePayment = _.get(phrase1Res.creditData, 'hasLatePayment', true)
  const isCurrentEmployed = _.get(phrase2Res.scoreData, 'isCurrentEmployed', false)
  return {
    kycResultData: {
      phraseOneStatus: KYC.STATUS.PASSED,
      phraseTwoStatus: KYC.STATUS.PASSED,
      score: _getScore(hasLatePayment, isCurrentEmployed)
    },
    resId: phrase1Res.resId,
    userData: phrase1Res.userData
  }
}

const verify = async (idPhoto, selfie, userId) => {
  const uuid = moment().format('YMDHms')
  const {kycResultData, userData, resId, errorMsg} = await  _getKycResult(idPhoto, selfie, userId, uuid)
  kycResultData.userId = userId
  const s3IdPhoto = await FileFacade.put(
    `user-${userId}/${uuid}/id-${idPhoto.name}`,
    idPhoto.data,
    {destination: STORAGE.KYC})
  const s3PhotoWithId = await FileFacade.put(
    `user-${userId}/${uuid}/selfie-${idPhoto.name}`,
    selfie.data,
    {destination: STORAGE.KYC})

  kycResultData.idUrl = s3IdPhoto.Location
  kycResultData.selfieUrl = s3PhotoWithId.Location
  kycResultData.status = errorMsg
  saveResult(kycResultData, resId, KYC_PROVIDER.KALAPA)
  if (errorMsg) throw httpError(400, errorMsg)
  return {
    userData,
    score: kycResultData.score,
  }
}

const mapping = (data) => {
  const response = data.KalapaResponse
  return {
    KMatching: response.selfieCheckMatched,
    KisPhotoUnique: response.fraudCheckFacesMatched,
    KAntifraud: response.score,
    HasBadDebt: response.hasBadDebt,
    Salary: response.salaryLevel,
    HasLate: response.hasLatePayment,
    MedicalCard: response.medicalNumber ? response.medicalNumber.substring(0, 2) : '',
    isCurrentEmployed: response.isCurrentEmployed,
    KIsTaxPayer: 'N/A',
    KCareer: 'N/A',
    Brief: 'N/A',
  }
}

module.exports = {
  verify,
  mapping
}