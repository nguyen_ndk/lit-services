const _ = require('lodash')
const httpError = require('http-errors')
const {facade: {KycFacade, Log}, providers: {KYC_PROVIDER}} = require('lit-utils')
const saveCounting = require('../save-counting')
const saveResponse = require('../save-reponse')
const uploadResponse = require('../upload-response')
const {errorTags, KYC, KALAPA} = require('lit-constants')

const provider = KYC_PROVIDER.KALAPA

/**
 * @param data
 * @param data.salaryLevel: integer
 * @returns void
 */
const verify = (data) => {
  if (data.salaryLevel !== 0 && data.salaryLevel <= KALAPA.MINIMUM_SALARY_LEVEL) {
    throw httpError(400, errorTags.UNDER_MINIMUM_SALARY_LEVEL)
  }
}
const _jsonPath = (userId, uuid) => `user-${userId}/${uuid}/score.json`
module.exports = async (id, userId, resId, uuid) => {
  const {data, api} = await KycFacade.provider(provider).userScore(id)
  const mapped = KycFacade.provider(provider).mappingScore(data)
  const file = await uploadResponse(data, _jsonPath(userId, uuid))
  mapped.scoreUrl = file.Location
  await saveResponse.update(mapped, provider, resId)
  let error
  try {
    verify(data)
  } catch (e) {
    error = e
  }
  saveCounting(api, provider, userId, _.get(error, 'message', KYC.COUNTING_STATUS.SUCCESS))
  if (error) return {errorMsg: error.message, passed: false}
  return {data, passed: true}
}