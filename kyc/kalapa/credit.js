const _ = require('lodash')
const httpError = require('http-errors')
const {facade: {KycFacade, Log}, providers: {KYC_PROVIDER}} = require('lit-utils')
const saveCounting = require('../save-counting')
const saveResponse = require('../save-reponse')
const uploadResponse = require('../upload-response')
const {errorTags, KYC} = require('lit-constants')

const provider = KYC_PROVIDER.KALAPA

/**
 * @param data
 * @param data.hasBadDebt: boolean
 * @returns void
 */
const verify = (data) => {
  if (data.hasBadDebt) {
    throw httpError(400, errorTags.HAD_BAD_DEBT)
  }
}

const _jsonPath = (userId, uuid) => `user-${userId}/${uuid}/credit.json`
module.exports = async (id, userId, resId, uuid) => {
  const {data, api} = await KycFacade.provider(provider).creditInfo(id)
  const mapped = KycFacade.provider(provider).mappingCredit(data)
  const file = await uploadResponse(data, _jsonPath(userId, uuid))
  mapped.creditUrl = file.Location
  await saveResponse.update(mapped, provider, resId)
  let error
  try {
    verify(data)
  } catch (e) {
    error = e
  }
  saveCounting(api, provider, userId, _.get(error, 'message', KYC.COUNTING_STATUS.SUCCESS))
  if (error) return {errorMsg: error.message, passed: false}
  return {data, passed: true}
}