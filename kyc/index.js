const kalapa = require('./kalapa')
const {KycResultRepo, UserRepo} = require('lit-repositories')
const {KYC, errorTags, USER} = require('lit-constants')
const {facade: {Log}} = require('lit-utils')

const moment = require('moment')
const httpError = require('http-errors')
const {Sms} = require('lit-notifications')

const updateUserCompletion = require('../update-user-completion')
const _getProcessor = () => {
  return kalapa
}

const verify = async (idPhoto, selfie, user) => {
  const processor = _getProcessor()
  try {
    const {userData} = await processor.verify(idPhoto, selfie, user.id)
    userData.isProcessingKYC = false
    await UserRepo.update(user.id, userData, {getModel: false})
    updateUserCompletion(user.id, USER.COMPLETION.ID)
  } catch (e) {
    await UserRepo.update(user.id, {isProcessingKYC: false}, {getModel: true})
    Log.error(`UPDATE_USER_ID_PHOTO USER ${user.id}`, e)
    Sms.sendFailedKyc(user)
  }
}

const downloadResponse = async () => {}

const isAllowToUpdate = async (userId) => {
  const result = await KycResultRepo.findLatestByUserId(userId)
  if (result && result.phraseTwoStatus === KYC.STATUS.PASSED &&
    moment(result.createdAt).isAfter(moment().add(KYC.UPDATE_GAP_DAY, 'd'))) {
    throw httpError(403, errorTags.NOT_ALLOW)
  }
}

const mapping = (data) => {
  const processor = _getProcessor()
  const result = processor.mapping(data)
  result.KSco = data.score
  result.phraseOneStatus = data.phraseOneStatus
  result.phraseTwoStatus = data.phraseTwoStatus
  result.createdAt = data.createdAt
  return result
}

module.exports = {
  verify,
  downloadResponse,
  isAllowToUpdate,
  mapping,
}