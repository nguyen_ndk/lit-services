const {facade: {FileFacade, Log}} = require('lit-utils')

module.exports = (response, path) => {
  return FileFacade.uploadKycResponse(path, response)
    .catch(e => {
      Log.error(`UPLOAD_KYC_RESPONSE PATH ${path}`, e)
      return 'Failed to upload'
    })
}