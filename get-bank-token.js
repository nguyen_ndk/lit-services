const httpError = require('http-errors')
const {PaymentMethodRepo} = require('lit-constants')
const {errorTags} = require('lit-constants')
module.exports = async userId => {
  const paymentMethod = await PaymentMethodRepo.findActiveByUserId(userId)
  if (!paymentMethod || !paymentMethod.bankToken) throw httpError(403, errorTags.MISSING_ACTIVE_PAYMENT_METHOD)
  return paymentMethod.bankToken
}