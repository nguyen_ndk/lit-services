const {
  UserCreditHistoryRepo
} = require('lit-repositories')
const {
  CREDIT_HISTORY_TYPE
} = require('lit-constants')
module.exports = async (purchaseId, amount, userId, transaction, type) => {
  const userCredit = {
    userId,
    type: type || CREDIT_HISTORY_TYPE.INSTALL_DEDUCT,
    amount,
    purchaseId,
  }
  await UserCreditHistoryRepo.create(userCredit, {transaction})
}