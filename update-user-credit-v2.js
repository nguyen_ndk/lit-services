const {UserCreditHistoryRepo, UserRepo} = require('lit-repositories')
const {facade: {DBFacade, Log}} = require('lit-utils')
const httpError = require('http-errors')
const {CREDIT_HISTORY_TYPE, errorTags} = require('lit-constants')

const _saveHistory = async (purchaseId, amount, userId, type, transaction) => {
  const userCredit = {
    userId,
    type: type || CREDIT_HISTORY_TYPE.INSTALL_DEDUCT,
    amount,
    purchaseId,
  }
  await UserCreditHistoryRepo.create(userCredit, {transaction})
}

const _getEndCredit = (currentCredit, changedAmount, type) => {
  switch (type) {
    case CREDIT_HISTORY_TYPE.CREDIT:
      return currentCredit + changedAmount
    case CREDIT_HISTORY_TYPE.INSTALL_DEDUCT:
      return currentCredit - changedAmount
    case CREDIT_HISTORY_TYPE.BLOCK:
      return currentCredit - changedAmount
    default:
      throw httpError(400, errorTags.INVALID_CREDIT_HISTORY_TYPE)
  }
}

module.exports = async (userId, changedAmount, purchaseId, type) => {
  const logTag = `UPDATE_USER_CREDIT USER ${userId} PURCHASE ${purchaseId} AMOUNT ${changedAmount}`
  const transaction = await DBFacade.transaction()
  try {
    const user = await UserRepo.selectForUpdate(userId, transaction)
    const endCredit = _getEndCredit(user.credit, changedAmount, type)
    if (endCredit < 0 ) {
      Log.error(logTag, `NEGATIVE_USER_CREDIT ${endCredit}`)
    }
    await Promise.all([
      UserRepo.update(userId, {credit: endCredit}, {transaction}),
      _saveHistory(purchaseId, changedAmount, userId, type, transaction)
    ])
    await transaction.commit()
  } catch (e) {
    await transaction.rollback()
    Log.error(logTag, e)
  }
}