const {facade: {Log, DBFacade}} = require('lit-utils')
const {UserCreditHistoryRepo, UserRepo, KycResultRepo, PaymentMethodRepo} = require('lit-repositories')
const {USER, PAYMENT_METHOD, CREDIT_HISTORY_TYPE, LIMIT_BY_TIER} = require('lit-constants')
const _ = require('lodash')

const getUserTier = async (score, pms) => {
  let tier = USER.TIER.T4

  //TODO REPLACE THE SCORE WHEN DECIDED BY UPPER MANAGEMENT
  if (score >= 50){
    tier = USER.TIER.T3
  }
  let creditCap = 0
  _.forEach(pms, pm => {
    let tmp = 0
    switch (pm.type) {
      case PAYMENT_METHOD.TYPE.CREDIT_CARD:
        tmp = LIMIT_BY_TIER[tier][PAYMENT_METHOD.TYPE.CREDIT_CARD]
        break
      case PAYMENT_METHOD.TYPE.DEBIT_CARD:
        tmp = LIMIT_BY_TIER[tier][PAYMENT_METHOD.TYPE.DEBIT_CARD]
        break
      case PAYMENT_METHOD.TYPE.ATM:
        tmp = LIMIT_BY_TIER[tier][PAYMENT_METHOD.TYPE.ATM]
        break
    }
    if (tmp > creditCap) creditCap = tmp
  })

  const result = {
    tier: tier,
    creditCap,
    credit: creditCap
  }
  return result
}

const haveApproveCredit = user => {
  return user.tier || user.credit
}

module.exports = async (userId) => {
  const logTag = `APPROVE_USER_CREDIT USER ${userId}`
  const user = await UserRepo.findById(userId)
  const pms = await PaymentMethodRepo.findByUserId(userId)
  if (_.isEmpty(pms)) {
    Log.error(`${logTag} PAYMENT_METHODS_EMPTY`)
    return
  }
  const latestPassedKyc = await KycResultRepo.findLatestPassedByUserId(userId)
  if (haveApproveCredit(user)) return
  const saveData = await getUserTier(latestPassedKyc.score, pms)
  const t = await DBFacade.transaction()
  try {
    Log.info(`${logTag} SAVE`, saveData)
    await UserRepo.update(user.id, saveData, {transaction: t, getModel: false})

    const creditHistoryData = {
      userId: user.id,
      type: CREDIT_HISTORY_TYPE.CREDIT,
      amount: saveData.credit,
    }
    await UserCreditHistoryRepo.create(creditHistoryData, {transaction: t})
    await t.commit()
  } catch (e) {
    Log.error(logTag, e)
    await t.rollback()
  }
}