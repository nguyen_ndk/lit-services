const httpError = require('http-errors')
const _ = require('lodash')
const moment = require('moment')
const {facade: {Log}} = require('lit-utils')
const {PurchaseRepo} = require('lit-repositories')
const {errorTags, PURCHASE_PAYMENT_STATUS, DEFAULT_DATETIME_FORMAT, INSTALLMENT} = require('lit-constants')
module.exports = async (id, calcData, userId, payeeAgent, transaction, pmId) => {
  const logTag = `USER_CONFIRM_PURCHASE UPDATE_PURCHASE_STATUS user: ${id}`
  Log.info(`${logTag} START`)
  const saveData = {
    paymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
    userId,
    totalInstallments: INSTALLMENT.NUMBER,
    payeeAgent,
    payFirstInstallmentAt: moment().format(DEFAULT_DATETIME_FORMAT),
    paidInstallments: 1,
    dueInstallments: 1,
    paymentMethodId: pmId,
    dueGap: INSTALLMENT.DUE_GAP,
    adminFeeAmount: calcData.adminFeeAmount,
    promotionId: _.get(calcData, 'promoData.promotionId'),
    amountDiscount: _.get(calcData, 'promoData.discountAmount'), //TODO rename this
    amount: _.get(calcData, 'promoData.amountCharge'),
  }
  const amount = _.get(calcData, 'promoData.amountCharge')
  if (amount) saveData.amount = amount
  const res = await PurchaseRepo.update(id, saveData, {getModel: false, transaction})
  if (!res) {
    Log.error(logTag, saveData)
    throw httpError(500, errorTags.SERVER_ERROR)
  }
}