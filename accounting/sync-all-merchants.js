const {MerchantRepo, ConfigurationRepo} = require('lit-repositories')
const _ = require('lodash')
const {facade: {Log, Accounting}} = require('lit-utils')
const async = require('async')
const _buildContact = (contacts, merchant) => {
  contacts.push(Accounting.buildMerchantData(merchant))
  return contacts
}

const _updateMerchantFinanceId = async (merchants) => {
  if (!_.isEmpty(merchants)) {
    await async.eachLimit(merchants, 10, (merchant, next) => {
      MerchantRepo.updateFinanceId(merchant.contactNumber, merchant.contactID)
        .then(() => {
          setTimeout(() => {next()}, 500)
        })
        .catch(e => {
          Log.error(`ACCOUNTING_SYNC_MERCHANT UPDATE_FINANCE_ID MERCHANT ${merchant.contactNumber}`, e)
          next()
        })
    })
  }
}

module.exports = async () => {
  const authKey = Accounting.getAuthKey()
  const configuration = await ConfigurationRepo.getAccountingAuth(authKey)
  let auth = configuration.value
  const cursor = MerchantRepo.cursorNotSyncToAccounting()
  let stop = false
  do {
    const merchants = await cursor.next()
    if (!_.isEmpty(merchants)) {
      const contacts = _.reduce(merchants, _buildContact, [])
      const {data, newAuth} = await Accounting.syncMerchants(contacts, auth)
      if (newAuth) auth = newAuth
      await _updateMerchantFinanceId(data.contacts)
    } else {
      stop = true
    }
    if (cursor.noMore) stop = true
  } while (!stop)
  ConfigurationRepo.updateAccountingAuth(authKey, auth)
}