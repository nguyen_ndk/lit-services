const {UserRepo, PurchaseRepo, MerchantRepo} = require('lit-repositories')
module.exports = async () => {
  const [purchase, merchant, user] = await Promise.all([
    PurchaseRepo.countNotSyncToAccounting(),
    MerchantRepo.countNotSyncToAccounting(),
    UserRepo.countNotSyncToAccounting(),
  ])
  return {purchase, merchant, user}
}