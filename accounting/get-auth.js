const {ConfigurationRepo} = require('lit-repositories')
const {facade: {Accounting}} = require('lit-utils')
module.exports = async () => {
  const authKey = Accounting.getAuthKey()
  const configuration = await ConfigurationRepo.getAccountingAuth(authKey)
  return configuration.value
}