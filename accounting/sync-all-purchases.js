const {PurchaseRepo, ConfigurationRepo, PurchaseAccountingRepo, PurchaseInstallmentRepo} = require('lit-repositories')
const _ = require('lodash')
const {facade: {Log, Accounting}} = require('lit-utils')
const async = require('async')

const _updateFinanceIds = (financeIds, purchaseId) => {
  PurchaseAccountingRepo.bulkCreate(financeIds)
    .catch(e => {
      Log.error(`ACCOUNTING_SYNC_PURCHASE UPDATE_PURCHASE_FINANCE_ID PURCHASE ${purchaseId}`, e)
    })
}

const _delayNext = (next) => {
  setTimeout(() => {next()}, 5000)
}

module.exports = async () => {
  const authKey = Accounting.getAuthKey()
  const configuration = await ConfigurationRepo.getAccountingAuth(authKey)
  let auth = configuration.value
  const cursor = PurchaseRepo.cursorNotSyncToAccounting()
  let stop = false
  do {
    const purchases = await cursor.next()
    if (!_.isEmpty(purchases)) {
      await async.eachLimit(purchases, 5, (purchase, next) => {
        PurchaseInstallmentRepo.findByPurchaseId(purchase.id)
          .then(installments => {
            return Accounting.syncPurchase(purchase, installments, auth)
          })
          .then(async result => {
            _updateFinanceIds(result.financeIds, purchase.id)
            if (result.newAuth) auth = result.newAuth
            await PurchaseRepo.markSyncAccounting(purchase.id)
            _delayNext(next)
          })
          .catch(e => {
            Log.error(`ACCOUNTING_SYNC_PURCHASE CALL_XERO_API_FAILED PURCHASE ${purchase.id}`, e)
            _delayNext(next)
          })
      })
    } else {
      stop = true
    }
    if (cursor.noMore) {
      stop = true
    }
  } while (!stop)
  ConfigurationRepo.updateAccountingAuth(authKey, auth)
}