const {facade: {Log, Accounting, PaymentGatewayFacade}} = require('lit-utils')
const {UserRepo} = require('lit-repositories')
const getAuth = require('./get-auth')

module.exports = (amount, pm, userId, desc) => {
  const fee = PaymentGatewayFacade.provider(pm.gateway).calcFee(amount, pm.type)
  Promise.all([getAuth(), UserRepo.findById(userId)])
    .then(([auth, user]) => {
      if (!user.financeId) {
        Log.error(`SYNC_PAYMENT_GATEWAY_FEE MISSING_USER_FINANCE_ID USER ${user.id}`)
        return
      }
      return Accounting.syncPaymentGatewayFee(desc, fee, user, auth)
    })
    .catch(e => {
      Log.error(`SYNC_PAYMENT_GATEWAY_FEE USER ${userId}`, e)
    })
}