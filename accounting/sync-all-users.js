const {UserRepo, ConfigurationRepo} = require('lit-repositories')
const _ = require('lodash')
const {facade: {Log, Accounting}} = require('lit-utils')
const async = require('async')
const _buildContact = (contacts, user) => {
  contacts.push(Accounting.buildUserData(user))
  return contacts
}

const _updateUsersFinanceId = (users) => {
  if (!_.isEmpty(users)) {
    return async.eachLimit(users, 10, (user, next) => {

      UserRepo.updateFinanceId(user.contactNumber, user.contactID)
        .then(() => {
          setTimeout(() => {next()}, 500)
        })
        .catch(e => {
          Log.error(`ACCOUNTING_SYNC_USER UPDATE_FINANCE_ID USER ${user.contactNumber}`, e)
          next()
        })
    })
  }
}

module.exports = async () => {
  const authKey = Accounting.getAuthKey()
  const configuration = await ConfigurationRepo.getAccountingAuth(authKey)
  let auth = configuration.value
  const cursor = UserRepo.cursorNotSyncToAccounting()
  let stop = false
  do {
    const users = await cursor.next()
    if (!_.isEmpty(users)) {
      const contacts = _.reduce(users, _buildContact, [])
      const {data, newAuth} = await Accounting.syncUsers(contacts, auth)
      if (newAuth) auth = newAuth
      await _updateUsersFinanceId(data.contacts)
    } else {
      stop = true
    }
    if (cursor.noMore) stop = true
  } while (!stop)
  ConfigurationRepo.updateAccountingAuth(authKey, auth)
}