const httpError = require('http-errors')
const moment = require('moment')
const {facade: {Log}} = require('lit-utils')
const {PurchaseRepo} = require('lit-repositories')
const {errorTags, PURCHASE_PAYMENT_STATUS, DEFAULT_DATETIME_FORMAT, INSTALLMENT} = require('lit-constants')
module.exports = async (id, totalInstallments, userId, payeeAgent, transaction, pmId, adminFeeAmount, promoData) => {
  const logTag = `USER_CONFIRM_PURCHASE UPDATE_PURCHASE_STATUS user: ${id}`
  Log.info(`${logTag} START`)
  const saveData = {
    paymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
    amount: promoData.amountCharge,
    promotionId: promoData.promotionId,
    amountDiscount: promoData.discountAmount,
    userId,
    totalInstallments,
    payeeAgent,
    payFirstInstallmentAt: moment().format(DEFAULT_DATETIME_FORMAT),
    paidInstallments: 1,
    dueInstallments: 1,
    paymentMethodId: pmId,
    dueGap: INSTALLMENT.DUE_GAP,
    adminFeeAmount
  }
  const res = await PurchaseRepo.update(id, saveData, {getModel: false, transaction})
  if (!res) {
    Log.error(logTag, saveData)
    throw httpError(500, errorTags.SERVER_ERROR)
  }
}