const async = require('async')
const moment = require('moment')
const _ = require('lodash')
const {facade: {DBFacade, Log}} = require('lit-utils')
const {
  PurchaseLineRepo,
  PaymentMethodRepo,
  PaymentGatewayRequestRepo,
  PurchaseInstallmentRepo,
  UserRepo,
  PurchaseRepo
} = require('lit-repositories')
const charge = require('../payment/direct/charge')
const {QUERY_METHOD_MAPPING, PAYMENT_METHOD, DEFAULT_DATETIME_FORMAT, CREDIT_HISTORY_TYPE, PURCHASE} = require('lit-constants')
const {literal} = require('sequelize')
const createPurchaseLine = require('../purchase-line/create-purchase-line')
const failedInstallmentByUserId = require('../installment/failed-installments-by-user-id')
const validateInstallmentByUserId = require('../installment/validate-installments-by-user-id')
const suspendUser = require('../user/suspend-user')
const unSuspendUser = require('../unsuspend-user')
const updateUserCredit = require('../update-user-credit-v2')
const {Email} = require('lit-notifications')
const {Purchase} = require('lit-models')




const isDue = (balanceDue) => {
  if (balanceDue.amountDue > 0) {
    return true
  }

  return false
}

const getPaymentMethods = async (userId, options = {}) => {
  let whereQuery = {
    userId: userId,
    status: {
      [QUERY_METHOD_MAPPING.IN]: [PAYMENT_METHOD.STATUS.PRIMARY, PAYMENT_METHOD.STATUS.SECONDARY]
    },
    type: {
      [QUERY_METHOD_MAPPING.IN]: [PAYMENT_METHOD.TYPE.CREDIT_CARD, PAYMENT_METHOD.TYPE.DEBIT_CARD]
    }
  }

  if (_.get(options, 'paymentMethodId', false)) {
    whereQuery.id = _.get(options, 'paymentMethodId')
  }

  //Get list of payment method per user
  let paymentMethods = await PaymentMethodRepo.findAll(
    {
      where: whereQuery,
      order: literal('FIELD(status, "primary", "secondary")')
    }
  )


  return paymentMethods
}


module.exports = async (mode = 'default', options = {}) => {
  const t = await DBFacade.transaction()
  try {
    //List of purchases due
    const userBalanceDue = await PurchaseLineRepo.searchUserWithBalanceDue(t, options)
    // console.log(userBalanceDue)
    await async.eachLimit(userBalanceDue, 10, async (balanceDue) => {
      //If balance is Due
      if (!isDue(balanceDue)) {
        return false
      }

      let userId = balanceDue.userId
      let transactionId = null
      let paymentMethods = await getPaymentMethods(userId, options)
      let usedPaymentMethod = null
      //Loop every payment method available and break once one payment method is successful
      for (var i = 0; i < paymentMethods.length; i++) {
        let desc = 'Auto billing due balance at ' + moment().format(DEFAULT_DATETIME_FORMAT)
        try {
          usedPaymentMethod = paymentMethods[i]
          transactionId = await charge(paymentMethods[i], balanceDue.amountDue, desc, userId)
          break
        } catch (e) {
          console.log('KEEP GOING NEXT PAYMENT METHOD AVAILABLE')
        }
      }

      // console.log('----------------------------------------------')
      // console.log(transactionId)

      let user = await UserRepo.findById(userId)

      //
      if (transactionId === null && mode === 'default') {

        //FAILED PAYMENT
        await failedInstallmentByUserId(userId)

        //Failed purchase
        await PurchaseRepo.updateInProgressPurchaseWithFailedInstallment(userId)


        //SUSPEND USER
        await suspendUser(user)


        Email.sendFailedOverallBilling(
          user
        )

      } else if (transactionId != null) { //PAYMENT SUCCEED
        //Get Payment Gateway Request
        let pgr = await PaymentGatewayRequestRepo.findByTransactionId(transactionId)

        const userBalanceDueByPurchaseId = await PurchaseLineRepo.searchUserWithBalanceDueByPurchaseId(t, {
          userId: userId
        })

        // console.log('---------------------------------------------------------------------------------')
        // console.log(userBalanceDueByPurchaseId)
        // console.log(usedPaymentMethod)
        //For each payment have to be seperated by purchase ID
        await async.eachLimit(userBalanceDueByPurchaseId, 1, async (userPurchaseBalanceDue) => {
          // console.log('*********************************************************')
          // console.log(userPurchaseBalanceDue)
          await createPurchaseLine(pgr, userPurchaseBalanceDue)

          const conditions = {
            include: {
              model: Purchase
            },
            where: {
              userId: userId,
              purchaseId: userPurchaseBalanceDue.purchaseId,
              status: {
                [QUERY_METHOD_MAPPING.IN]: [
                  PURCHASE.INSTALLMENT_STATUS.DUE,
                  PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED,
                  PURCHASE.INSTALLMENT_STATUS.HARD_FAILED,
                ]
              }
            },
          }


          //Get list of installment to validate (For email) :
          let purchaseInstallmentList = await PurchaseInstallmentRepo.findAll(conditions)

          //Update due Installment to validated
          let updateMany = await validateInstallmentByUserId(userId, transactionId, userPurchaseBalanceDue.purchaseId, usedPaymentMethod.id)


          // console.log('************************************************')
          // console.log(purchaseInstallmentList)


          // console.log(userPurchaseBalanceDue.purchaseId)
          // console.log('==>')
          // console.log(updateMany)
          PurchaseRepo.payInstallmentsV2(await PurchaseRepo.findById(userPurchaseBalanceDue.purchaseId), updateMany)

          await updateUserCredit(userId, userPurchaseBalanceDue.amountDue, userPurchaseBalanceDue.purchaseId, CREDIT_HISTORY_TYPE.CREDIT)

          // console.log('----------------------------------------')

          //Send Success Email
          await async.eachLimit(purchaseInstallmentList, 1, async (purchaseInstallment) => {
            // console.log(purchaseInstallment)
            // console.log('INNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN')
            Email.sendSuccessOverallBilling(
              user,
              await PurchaseInstallmentRepo.findById(purchaseInstallment.id, {includePurchase:true})
            )
          })

        })



        //Unsuspend Purchase Payment Status
        await PurchaseRepo.updateFailedInstallmentPurchaseToInprogress(userId)

        //Check if any purchase have completed all installments
        await PurchaseRepo.updateInProgressPurchaseToCompleted(userId)


        //UNSUSPEND USER
        await unSuspendUser(user)
      }
    })

    await t.commit()
  } catch (e) {
    Log.error('ERROR OVERALL_BILLING', {e})
    await t.rollback()
  }
}