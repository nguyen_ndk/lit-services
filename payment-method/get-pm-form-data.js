const uuid = require('uuid')
const {PaymentGatewayRequestRepo} = require('lit-repositories')
const {facade: {PaymentGatewayFacade}} = require('lit-utils')
const {PAYMENT_GATEWAY_REQUEST, WINDOW_TYPE, INIT_TOKEN_FEE} = require('lit-constants')
module.exports = async (req) => {
  let transactionId = uuid.v4()
  await PaymentGatewayRequestRepo.create({
    type: PAYMENT_GATEWAY_REQUEST.TYPE.ADD_PAYMENT_METHOD,
    gateway: req.gateway,
    transactionId,
    userId: req.user.id,
    status: PAYMENT_GATEWAY_REQUEST.STATUS.INIT,
    amount: INIT_TOKEN_FEE,
  })
  const ctx = {
    tranId: transactionId,
    userEmail: req.user.email,
    userId: req.user.id,
    ip: req.ip,
    windowType: req.windowType || WINDOW_TYPE.DESKTOP,
    paymentMethodType: req.paymentMethodType,
  }
  return await PaymentGatewayFacade.provider(req.gateway).getPMFormData(INIT_TOKEN_FEE, ctx)
}