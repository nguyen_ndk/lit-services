const _ = require('lodash')
const {PaymentMethodRepo} = require('lit-repositories')
const {USER, DEFAULT_CURRENCY} = require('lit-constants')
const {facade: {Log}, providers: {PAYMENT_GATEWAY_PROVIDER}} = require('lit-utils')
const updateUserCompletion = require('../update-user-completion')
const refund = require('../payment/refund')

const _refund = (paymentGatewayReq, paymentMethodType, extractedData) => {
  const ctx = {
    tranId: paymentGatewayReq.transactionId,
    userId: paymentGatewayReq.userId,
    gateway: PAYMENT_GATEWAY_PROVIDER.EPAY,
    gatewayTranId: extractedData.gatewayTranId,
    paymentMethodType
  }
  refund(paymentGatewayReq.amount, DEFAULT_CURRENCY, ctx)
    .catch(e => {
      Log.error(`WEBHOOK_TOKEN REFUND TRANSACTION ${paymentGatewayReq.transactionId}`, e)
    })
}
module.exports = async (extractedData, paymentGatewayReq) => {
  const pms = await PaymentMethodRepo.findByUserId(paymentGatewayReq.userId)
  const isFirst = _.isEmpty(pms)
  try {
    const pmData = {
      userId: paymentGatewayReq.userId,
      ccCropped: extractedData.maskedCard,
      cardBrand: extractedData.cardBrand,
      bankToken: extractedData.token,
      type: extractedData.type,
      gateway: extractedData.gateway,
    }
    const pm = await PaymentMethodRepo.addActive(pmData)
    if (isFirst) {
      updateUserCompletion(paymentGatewayReq.userId, USER.COMPLETION.CARD)
    }
    _refund(paymentGatewayReq, pmData.type, extractedData)
    return pm
  } catch (e) {
    Log.error(`WEBHOOK_TOKEN SAVE_DATA payment_gateway_request ${paymentGatewayReq.id}`, extractedData, e)
    throw e
  }
}