const httpError = require('http-errors')
const _ = require('lodash')
const moment = require('moment')
const {facade: {Log}} = require('lit-utils')
const {PurchaseRepo} = require('lit-repositories')
const {errorTags, DEFAULT_DATETIME_FORMAT, INSTALLMENT} = require('lit-constants')
module.exports = async (id, calcData, userId, payeeAgent, pmId) => {
  const logTag = `USER_CONFIRM_PURCHASE UPDATE_PURCHASE_DATA user: ${id}`
  Log.info(`${logTag} START`)
  const saveData = {
    userId,
    totalInstallments: INSTALLMENT.NUMBER,
    payeeAgent,
    payFirstInstallmentAt: moment().format(DEFAULT_DATETIME_FORMAT),
    paidInstallments: 1,
    dueInstallments: 1,
    paymentMethodId: pmId,
    dueGap: INSTALLMENT.DUE_GAP,
    adminFeeAmount: calcData.adminFeeAmount,
    amount: _.get(calcData, 'promoData.amountCharge'),
    promotionId: _.get(calcData, 'promoData.promotionId'),
    amountDiscount: _.get(calcData, 'promoData.discountAmount'), //TODO rename this
  }
  const res = await PurchaseRepo.update(id, saveData, {getModel: false})
  if (!res) {
    Log.error(logTag, saveData)
    throw httpError(500, errorTags.SERVER_ERROR)
  }
}