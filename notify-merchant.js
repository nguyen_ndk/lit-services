const {axios} = require('axios')
const {PURCHASE} = require('lit-constants')
const {facade: {Log}} = require('lit-utils')

module.exports =  (p, m) => {
  const body = {
    clientId: m.clientId,
    amount: p.amount,
    transactionId: p.transactionId,
    orderId: p.merchantOrderId,
    requestId: p.merchantRequestId,
    errorMessage: '',
    errorCode: 0,
    payeeAgent: PURCHASE.PAYEE_AGENT.QR,
    responseTime: Date.now() / 1000,
  }
  axios.post(p.notifyUrl, body)
    .catch(e => {
      Log.error('MERCHANT_NOTIFY ERROR', e, body)
    })
}