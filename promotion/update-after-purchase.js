const {
  PromotionRepo
} = require('lit-repositories')
const { UserHasPromotion } = require('lit-models')
const {facade: {Log}} = require('lit-utils')

module.exports = async (promoData, user) => {
  try {
    Log.info(`START_UPDATE_PROMOTION`, promoData, user)
    let promotion = await PromotionRepo.findById(promoData.promotionId)
    if (promotion && promoData.isApprove){
      promotion.quantityUsed += 1
      promotion.budgetUsed += promoData.discountAmount
      let userPromotion = await UserHasPromotion.findOne({
        where: {
          userId: user.id,
          promotionId: promoData.promotionId
        }
      })
      if (!promotion.isCheckout){
        userPromotion.discountAmount += promoData.discountAmount
        userPromotion.amountVoucherUsed +=1
        if (userPromotion.amountVoucherUsed === userPromotion.amountVoucher){
          userPromotion.status = true
        }
        await userPromotion.save()
      } else {
        promotion.maxVoucherCanClaim -=1
        if (!userPromotion){
          await UserHasPromotion.create({
            userId: user.id,
            promotionId: promoData.promotionId,
            amountVoucherUsed : 1,
            amountVoucher: promotion.maxQtyPerUser,
            discountAmount: promoData.discountAmount,
            status: promotion.maxQtyPerUser == 1
          })
        } else {
          userPromotion.amountVoucherUsed += 1
          userPromotion.discountAmount += promoData.discountAmount
          if (userPromotion.amountVoucherUsed === userPromotion.amountVoucher){
            userPromotion.status = true
          }
          await userPromotion.save()
        }
      }
      await promotion.save()
    }
  } catch (e) {
    Log.error(`UPDATE_USER_PROMOTION USER ${user.id}`, promoData, e)
  }

}
