const moment = require('moment')
const {PurchaseInstallmentRepo} = require('lit-repositories')
const {QUERY_METHOD_MAPPING, PURCHASE, DEFAULT_DATETIME_FORMAT} = require('lit-constants')
const {facade: {Log}} = require('lit-utils')
module.exports = (purchaseId, transaction, transactionId) => {
  const logTag = `UPDATE_FAILED_INSTALLMENTS purchase: ${purchaseId}`
  Log.info(`${logTag} START`)
  const options = {
    transaction,
    where: {
      purchaseId,
      status: {
        [QUERY_METHOD_MAPPING.IN]: [
          PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED,
          PURCHASE.INSTALLMENT_STATUS.HARD_FAILED,
          PURCHASE.INSTALLMENT_STATUS.DUE,
        ]
      }
    }
  }
  const saveData = {
    status: PURCHASE.INSTALLMENT_STATUS.VALIDATED,
    transactionId,
    paidAt: moment().format(DEFAULT_DATETIME_FORMAT)
  }
  return PurchaseInstallmentRepo.updateMany(saveData, options)
}