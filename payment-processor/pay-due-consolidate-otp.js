const getMultiPurchasePaymentFormData = require('../payment/3d-sercure/get-multi-purchase-payment-form-data')

const PayDueConsolidateDirect = require('./pay-due-consolidate-direct')
class PayDueConsolidateOtp extends PayDueConsolidateDirect {
  static async update(result) {
    return {data: result, needOtp: true}
  }
  static webhook(paymentGatewayReq, purchase, pmId, user) {
    return super.update(
      paymentGatewayReq.transactionId,
      purchase,
      pmId,
      paymentGatewayReq.amount,
      {user, installmentNumber: paymentGatewayReq.installmentNumber})
  }

  static processPayment(req, chargeAmount, pm, desc) {
    return getMultiPurchasePaymentFormData(req, chargeAmount, pm, desc)
  }
}

module.exports = PayDueConsolidateOtp