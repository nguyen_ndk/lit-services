const {helpers, facade: {DBFacade, Log}} = require('lit-utils')
const {PurchaseRepo} = require('lit-repositories')
const {Email} = require('lit-notifications')
const updateUserCredit = require('../update-user-credit-v2')
const {CREDIT_HISTORY_TYPE, PURCHASE} = require('lit-constants')
const addSucceededFirstInstallment = require('../add-succeeded-first-installment')
const accountingSyncPurchase = require('../accounting/sync-purchase')
const getNewPurchaseFormData = require('../payment/3d-sercure/get-new-purchase-form-data')
const newPurchaseProcessing = require('../new-purchase-processing')
const updatePromotion = require('../promotion/update-after-purchase')
const PayDynamicQrDirect = require('./pay-dynamic-qr-direct')

class PayDynamicQrOtp extends PayDynamicQrDirect {

  static async update(result, purchase, pmId, calcData, req) {
    await newPurchaseProcessing(purchase.id, calcData, req.user.id, PURCHASE.PAYEE_AGENT.QR, pmId)
    return {data: result, needOtp: true}
  }

  static async webhook(paymentGatewayReq, purchase, pmId, user) {
    const t = await DBFacade.transaction()
    const {amount} = helpers.calcInstallment(purchase.amount, purchase.totalInstallments)
    try {
      const creditData = helpers.calcUserCreditAfterNewPurchase(user.credit, purchase.amount, amount)
      const [i] = await Promise.all([
        addSucceededFirstInstallment(paymentGatewayReq.amount,
          {purchaseId: purchase.id,
            transaction: t,
            paymentMethodId: pmId,
            userId: user.id,
            tranId: paymentGatewayReq.transactionId}),
        PurchaseRepo.markInProgress(purchase, paymentGatewayReq.transactionId, t)
      ])
      await t.commit()
      updateUserCredit(user.id, creditData.changed, purchase.id, CREDIT_HISTORY_TYPE.INSTALL_DEDUCT)
      Email.sendNewPurchase(user.email, creditData.newCredit, paymentGatewayReq.amount, purchase, purchase.Merchant)
      updatePromotion({
        promotionId: purchase.promotionId,
        discountAmount: purchase.amountDiscount,
        isApprove: purchase.promotionId !== 0
      }, user)
      accountingSyncPurchase(purchase.id, i)
    } catch (e) {
      await t.rollback()
      Log.error(`PAY_DYNAMIC_QR_DIRECT UPDATE PURCHASE ${purchase.id}`, e)
    }
  }

  static async processPayment(req, chargeAmount, pm, desc) {
    try {
      const result = await getNewPurchaseFormData(req, chargeAmount, pm, desc)
      return {isSuccess: true, data: result}
    } catch (e) {
      return {isSuccess: false, error: e}
    }
  }
}

module.exports = PayDynamicQrOtp