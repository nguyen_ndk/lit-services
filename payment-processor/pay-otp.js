const getPaymentFormData = require('../payment/3d-sercure/get-payment-form-data')

const PayDirect = require('./pay-direct')
class PayOtp extends PayDirect {
  static async update(result) {
    return {data: result, needOtp: true}
  }
  static webhook(paymentGatewayReq, purchase, pmId, user) {
    return super.update(
      paymentGatewayReq.transactionId,
      purchase,
      pmId,
      paymentGatewayReq.amount,
      {user, installmentNumber: paymentGatewayReq.installmentNumber})
  }

  static processPayment(req, chargeAmount, pm, desc) {
    return getPaymentFormData(req, chargeAmount, pm, desc)
  }
}

module.exports = PayOtp