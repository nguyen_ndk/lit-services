const {helpers, facade: {DBFacade, Log}} = require('lit-utils')
const {PurchaseRepo} = require('lit-repositories')
const {Email} = require('lit-notifications')
const _ = require('lodash')
// const httpError = require('http-errors')
const updateUserCredit = require('../update-user-credit-v2')
const {CREDIT_HISTORY_TYPE, PURCHASE} = require('lit-constants')
const validationService = require('../validation')
const charge = require('../payment/direct/charge-v2')
const {PromotionRepo, MerchantRepo} = require('lit-repositories')
const addSucceededFirstInstallment = require('../add-succeeded-first-installment')
const accountingSyncPurchase = require('../accounting/sync-purchase')
const newPurchaseSuccess = require('../new-purchase-success')
const updatePromotion = require('../promotion/update-after-purchase')
class PayDynamicQrDirect {
  static async getAmount(req, purchase) {
    let promoData = await PromotionRepo.verifyPurchase({userId: req.user.id, amount: purchase.originalAmount}, req.voucherCode)
    // if (req.voucherCode !== '' && !promoData.isApprove) throw httpError(400, promoData.message)
    const result = MerchantRepo.calcInstallment(promoData.amountCharge, purchase.Merchant)
    result.promoData = promoData
    return result
  }

  static getDesc(purchaseId, changeAmount) {
    return `Pay dynamic QR purchase: ${purchaseId} amount: ${changeAmount}`
  }

  static validate(paymentMethod, purchase, user) {
    validationService.notFoundPurchase(purchase)
    validationService.canStartPurchase(purchase)
    validationService.isPMPayable(paymentMethod, user)
    validationService.canUserMakeNewPurchase(user, purchase)
    const merchant = _.get(purchase, 'Merchant')
    validationService.canMerchantReceiveNewPurchase(merchant)
  }

  static async update(tranId, purchase, pmId, calcData, req) {
    const t = await DBFacade.transaction()
    try {
      const creditData = helpers.calcUserCreditAfterNewPurchase(req.user.credit, calcData.promoData.amountCharge, calcData.amount)
      const [i] = await Promise.all([
        addSucceededFirstInstallment(calcData.chargeAmount,
          {purchaseId: purchase.id,
            transaction: t,
            paymentMethodId: pmId,
            userId: req.user.id,
            tranId}),
        newPurchaseSuccess(purchase.id, calcData, req.user.id, PURCHASE.PAYEE_AGENT.QR, t, pmId)
      ])
      await t.commit()
      updateUserCredit(req.user.id, creditData.changed, purchase.id, CREDIT_HISTORY_TYPE.INSTALL_DEDUCT)
      Email.sendNewPurchase(req.user.email, creditData.newCredit, calcData.chargeAmount, purchase, purchase.Merchant)
      accountingSyncPurchase(purchase.id, i)
      updatePromotion(calcData.promoData, req.user)
      const updatePurchase = await PurchaseRepo.findById(purchase.id)
      const data = PurchaseRepo.getPublicData(updatePurchase)
      return {data, needOtp: false}
    } catch (e) {
      await t.rollback()
      Log.error(`PAY_DYNAMIC_QR_DIRECT UPDATE PURCHASE ${purchase.id}`, e)
      throw e
    }
  }
  static failedUpdate(purchaseId, userId) {
    PurchaseRepo.markFailed(purchaseId, userId).catch(e => {
      Log.error(`DYNAMIC_QR_PURCHASE FAILED_TO_MASK_FAILED PURCHASE ${purchaseId}`, e)
    })
  }

  static async processPayment(req, chargeAmount, pm, desc) {
    try {
      const tranId = await charge(req, chargeAmount, pm, desc)
      return {isSuccess: true, data: tranId}
    } catch (e) {
      return {isSuccess: false, error: e}
    }
  }
}

module.exports = PayDynamicQrDirect