module.exports = {
  PayDirect: require('./pay-direct'),
  PayOtp: require('./pay-otp'),
  PayDueConsolidateDirect: require('./pay-due-consolidate-direct'),
  PayDueConsolidateOtp: require('./pay-due-consolidate-otp'),
  PayDynamicQrDirect: require('./pay-dynamic-qr-direct'),
  PayDynamicQrOtp: require('./pay-dynamic-qr-otp'),
}