const {helpers, facade: {DBFacade, Log}} = require('lit-utils')
const {PurchaseRepo} = require('lit-repositories')
const getChargeAmountService = require('../get-charge-amount')
const updateFailedInstallments = require('../update-failed-installments')
const {Email} = require('lit-notifications')

const updateUserCredit = require('../update-user-credit-v2')
const {CREDIT_HISTORY_TYPE} = require('lit-constants')
const unSuspendUser = require('../unsuspend-user')
const addInstallmentService = require('../add-installments')
const addPurchaseLinePayment = require('../add-purchase-line-payment')
const validationService = require('../validation')
const charge = require('../payment/direct/charge-v2')

class PayDirect {
  static async getAmount(installmentNumber, purchase) {
    const due = await getChargeAmountService.getDue(purchase.id)
    // if the submitted installment number is not bigger than the the number of due installments, -> users only want to
    // pay due amount only
    if (this.isPayDueOnly(purchase.dueInstallments, installmentNumber)) return due
    const amount = PurchaseRepo.calcPurchaseInstallmentAmount(purchase.amount, purchase.totalInstallments)
    return due + (installmentNumber - purchase.dueInstallments) * amount
  }

  static isPayDueOnly(dueInstallments, installmentNumber) {
    return installmentNumber <= dueInstallments
  }

  static getDesc(purchaseId, changeAmount, installmentNumber) {
    return `Pay to installment ${installmentNumber} purchase: ${purchaseId} amount: ${changeAmount}`
  }

  static validate(paymentMethod, purchase, user) {
    validationService.purchaseUnprocessableState(purchase)
    validationService.notFoundPM(paymentMethod)
    validationService.purchaseBelongToUser(purchase, user)
    validationService.pmBelongToUser(paymentMethod, user)
  }

  static async getReturnCredit(purchase, installmentNumber) {
    const number = installmentNumber < purchase.dueInstallments ? purchase.dueInstallments : installmentNumber
    const {remaining} = helpers.calcPurchaseRemaining(
      number,
      purchase.paidInstallments,
      purchase.amount
    )
    return remaining
  }

  static async update(tranId, purchase, pmId, chargeAmount, req) {
    const t = await DBFacade.transaction()
    try {
      const returnCredit = await this.getReturnCredit(purchase, req.installmentNumber)
      const promises = [
        PurchaseRepo.payInstallments(purchase, req.installmentNumber, t),
        updateFailedInstallments(purchase.id, t, tranId)
      ]
      if (!this.isPayDueOnly(purchase.dueInstallments, req.installmentNumber)) {
        promises.push(addInstallmentService.bulkCreate(purchase, req.installmentNumber, pmId, tranId, t))
      }
      await Promise.all(promises)
      await t.commit()
      updateUserCredit(req.user.id, returnCredit, purchase.id, CREDIT_HISTORY_TYPE.CREDIT)
      Email.sendPaymentSuccess(req.user.email, returnCredit, chargeAmount, purchase)
      unSuspendUser(req.user)
      addPurchaseLinePayment(purchase, chargeAmount)
      const updatePurchase = await PurchaseRepo.findById(purchase.id)
      const data = PurchaseRepo.getPublicData(updatePurchase)
      return {data, needOtp: false}
    } catch (e) {
      await t.rollback()
      Log.error(`PAY_DIRECT UPDATE_PAY_NOW PURCHASE ${purchase.id}`, e)
    }
  }

  static processPayment(req, chargeAmount, pm, desc) {
    return charge(req, chargeAmount, pm, desc)
  }
}

module.exports = PayDirect