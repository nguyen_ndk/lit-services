const moment = require('moment')
const {DEFAULT_DATETIME_FORMAT, PURCHASE} = require('lit-constants')
const {PurchaseInstallmentRepo, PurchaseLineRepo} = require('lit-repositories')
module.exports =  async (amount, context = {}) => {
  const now = moment().format(DEFAULT_DATETIME_FORMAT)
  const saveData = {
    purchaseId: context.purchaseId,
    userId: context.userId,
    amount,
    installmentNumber: 1,
    paymentMethodId: context.paymentMethodId,
    status: PURCHASE.INSTALLMENT_STATUS.VALIDATED,
    paidAt: now,
    dueDate: now,
    transactionId: context.tranId
  }
  await PurchaseLineRepo.addFirstPayment(context.purchaseId, context.userId, amount)
  return await PurchaseInstallmentRepo.create(saveData, {transaction: context.transaction})
}