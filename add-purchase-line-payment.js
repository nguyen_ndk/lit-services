const {PurchaseLineRepo} = require('lit-repositories')
const {facade: {Log}} = require('lit-utils')

module.exports = (purchase, changedAmount) => {
  PurchaseLineRepo.addPayment(purchase, changedAmount)
    .catch(e => {
      Log.error(`ADD_PURCHASE_LINE_PAYMENT USER ${purchase.userId} PURCHASE ${purchase.id}`, e)
    })
}